Girder
Drift -name "DM1" -length 0.25 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM1" -synrad $quad_synrad -length 0.5 -strength [expr 0.01150520273*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM2" -length 41.19281534 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM2" -synrad $quad_synrad -length 0.5 -strength [expr -0.04606731829*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM3" -length 26.41346767 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM3" -synrad $quad_synrad -length 1.324089066 -strength [expr 0.1892026617*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM4" -length 2.931945 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM4" -synrad $quad_synrad -length 2.839104414 -strength [expr -0.3841798884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM5" -length 2.673538 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFH" -synrad $quad_synrad -length 0.5 -strength [expr 0.009272199853*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 75.59541 -angle 0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.00012*0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.01854439971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "HS3" -synrad $mult_synrad -type 3 -length 0 -strength [expr 1.881245668*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS11" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr -0.9269493163*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFH2" -synrad $quad_synrad -length 1 -strength [expr 0.01854439971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "OCT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*779.1005089*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS12" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr 4.123699771*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS4" -synrad $mult_synrad -type 3 -length 0 -strength [expr 1.111573112*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.01854439971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 75.59541 -angle 0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.00012*0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFH2" -synrad $quad_synrad -length 1 -strength [expr 0.01854439971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 75.59541 -angle 0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.00012*0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.01854439971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "HS5" -synrad $mult_synrad -type 3 -length 0 -strength [expr -6.450555465*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS21" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr 7.418093445*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFH2" -synrad $quad_synrad -length 1 -strength [expr 0.01854439971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "HS22" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr -4.058597058*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*779.1005089*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS6" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.5227415241*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.01854439971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 75.59541 -angle 0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.00012*0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFH" -synrad $quad_synrad -length 0.5 -strength [expr 0.009272199853*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Bpm
Quadrupole -name "QDV" -synrad $quad_synrad -length 0.5 -strength [expr -0.009272161388*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 75.59541 -angle -0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.00012*-0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.01854432278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS3" -synrad $mult_synrad -type 3 -length 0 -strength [expr -1.076812548*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS11" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr 4.91008432*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDV2" -synrad $quad_synrad -length 1 -strength [expr -0.01854432278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS12" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr 6.478273304*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT2" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*83.24674802*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS4" -synrad $mult_synrad -type 3 -length 0 -strength [expr -1.048340831*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.01854432278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 75.59541 -angle -0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.00012*-0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDV2" -synrad $quad_synrad -length 1 -strength [expr -0.01854432278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 75.59541 -angle -0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.00012*-0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.01854432278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS5" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.5226926741*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS21" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr 12.68068838*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDV2" -synrad $quad_synrad -length 1 -strength [expr -0.01854432278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS22" -synrad $mult_synrad -type 3 -length 0.7 -strength [expr -0.3509453723*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT2" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*83.24674802*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 74.89541 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS6" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.5708790946*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.01854432278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 75.59541 -angle -0.00012 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.00012*-0.00012/75.59541*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDV" -synrad $quad_synrad -length 0.5 -strength [expr -0.009272161388*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1" -length 0.75 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "Q1" -synrad $quad_synrad -length 0.5 -strength [expr 0.01475082762*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2" -length 31.35836948 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "Q2" -synrad $quad_synrad -length 0.5 -strength [expr -0.01647853595*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3" -length 151.172191 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT1" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-592.3480704*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1" -synrad $quad_synrad -length 3.260733907 -strength [expr 0.112091641*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D4" -length 5.3 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT0" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-68.71682786*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD0" -synrad $quad_synrad -length 2.673534313 -strength [expr -0.3027908613*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D5" -length 3.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
