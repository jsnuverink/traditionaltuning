import TuningAnalysis
reload(TuningAnalysis)

# clean all plots
def cleanPlots():
    figures=[manager.canvas.figure
            for manager in mpl._pylab_helpers.Gcf.get_all_fig_managers()]

    for i, figure in enumerate(figures):
        figure.clf()

cleanPlots()
import os.path

nrSeeds = 5 #for testing
#nrSeeds = 110
savePlots=False

iterations = 1
labels = ["1st","2nd","3rd"]
directories = ["../Iter1","../Iter2","../Iter3"]
colors = ['b','r','g','c','m','y','k']
b = []

for i in range(0,iterations) :
    cleanPlots()
    b.append(TuningAnalysis.TuningStudy(directory=directories[i],label=labels[i],color=colors[i],savePlots=savePlots))
    finishedSeeds = []
    missingSeeds = []
    for j in range(1,nrSeeds+1):
        fdir = directories[i] + "/seed_0.010_" + str(j) + "/"
        fname  = fdir + "Lumi.dat"
        if not os.path.isfile(fname):
            missingSeeds.append(j)
            continue
        finishedSeeds.append(j)
        a = TuningAnalysis.TuningStudySingle(directory=fdir,plots=True,knobPlot=True,seed = j, savePlots=savePlots)
        a.plot()

        b[i].Add(a)

    print 'finished seeds (', len(finishedSeeds), ') for', labels[i], 'iteration are ', finishedSeeds

    print 'missing seeds (', len(missingSeeds), ') for', labels[i], 'iteration are ', missingSeeds

c = TuningAnalysis.TuningStudyArray(directory="../Iter1-3/",savePlots=savePlots)

for i in range(0,iterations) :
    b[i].Analyse()
    c.Add(b[i])

c.Analyse()
