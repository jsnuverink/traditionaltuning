import TuningAnalysis
reload(TuningAnalysis)

# clean all plots
figures=[manager.canvas.figure
         for manager in mpl._pylab_helpers.Gcf.get_all_fig_managers()]

for i, figure in enumerate(figures):
    figure.clf()

a1 = TuningAnalysis.TuningStudySingle(directory="../def_output",plots=True,knobPlot=True)
a2 = TuningAnalysis.TuningStudySingle(directory="../def_output")

b = TuningAnalysis.TuningStudy(directory="../output")
b.Add(a1)
b.Add(a2)
b.Analyse()
