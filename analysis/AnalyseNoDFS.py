import TuningAnalysis
reload(TuningAnalysis)

# clean all plots
def cleanPlots():
    figures=[manager.canvas.figure
            for manager in mpl._pylab_helpers.Gcf.get_all_fig_managers()]

    for i, figure in enumerate(figures):
        figure.clf()

cleanPlots()
import os.path

nrSeeds = 10 # for testing
#nrSeeds = 110

iterations = 1
labels = ["DFS Knobs","DFS","DFS hybrid","DFS hybrid + 2nd"]
directories = ["../Iter1DFSKnobs","../Iter1","../Iter1DFSHybrid","../Iter1DFSHybrid-2nd"]
colors = ['b','r','g','k','c']
knobStages = [6,6,6,1]
knobs = [12,10,12,4]
b = []

for i in range(0,iterations):
    cleanPlots()
    b.append(TuningAnalysis.TuningStudy(directory=directories[i],label=labels[i],color=colors[i]))
    finishedSeeds = []
    missingSeeds = []
    for j in range(1,nrSeeds+1) :
        fdir = directories[i] + "/seed_0.010_" + str(j) + "/"
        fname  = fdir + "Lumi.dat"
        if not os.path.isfile(fname):
            missingSeeds.append(j)
            continue
        finishedSeeds.append(j)
        a = TuningAnalysis.TuningStudySingle(directory=fdir,plots=True,knobPlot=True,plotNumber=1,seed = j,nrKnobs=knobs[i],nrKnobStages=knobStages[i])

        b[i].Add(a)

    print 'finished seeds (', len(finishedSeeds), ') for', labels[i], 'iteration are ', finishedSeeds
    print 'missing seeds (', len(missingSeeds), ') for', labels[i], 'iteration are ', missingSeeds

c = TuningAnalysis.TuningStudyArray(directory="../DFSStudy/")

for i in range(0,iterations) :
    b[i].Analyse()
    c.Add(b[i])

c.Analyse()

LNoDFS2rel = array([ 0.39005817,  0.40254346,  1.06657729,  0.02630676,  0.02227331,
        0.0196954 ,  0.03660818,  0.33980766,  0.01773831,  0.52860468,
        0.2011033 ,  1.05321153,  0.89462034,  1.03028217,  0.12258559,
        0.8734202 ,  0.90637322,  0.84681295,  0.16299092,  0.98265458,
        0.22448612,  0.84237885,  0.76307797,  0.71684115,  0.85783342,
        0.67923458,  0.01312753,  1.02444142,  0.82940705,  0.56088875,
        0.26259268,  0.79343451,  0.31824264,  0.03419652,  0.87379302,
        0.93665573,  0.52191254,  0.89596881,  0.02055781,  0.01799862,
        0.901524  ,  1.007292  ,  0.01318348,  0.21488894,  0.73416244,
        0.01706674,  0.01655102,  0.87161959,  0.20252475,  0.93797512,
        0.63404481,  0.52965966,  0.02065279,  1.05688414,  0.63844454,
        0.01161018,  0.92225349,  0.94829227,  0.14535868,  1.13550549,
        0.03461904,  1.03002305,  1.04953627,  0.1582464 ,  0.02280167,
        1.0384259 ,  0.49995356,  0.01291439,  0.02966565,  0.64035092,
        0.50936644,  1.02436739,  1.00089336,  1.00631634,  1.06458366,
        0.73526237,  0.64702719,  0.82333363,  0.0110118 ,  0.03307702,
        0.02595049,  0.84127627,  0.03599819,  0.53416515,  0.01929033,
        0.30073627,  0.46369017,  0.79050753,  0.02863049,  0.22218868,
        0.24928324,  0.02682169,  0.88038203,  0.0227366 ,  0.00962737,
        0.79790298,  1.02012895,  0.01297685,  0.0238046 ,  0.96052108,
        0.02632   ,  0.96161573,  0.03370235,  0.86326698,  0.16120432,
        0.03841883,  0.8973781 ,  1.0519741 ,  0.65748183])

import pylab as pl
import numpy as np

pl.figure(8)
# lots of bins to make transition smooth
values, base = np.histogram(LNoDFS2rel, bins=1000)
#evaluate the cumulative
cumulative = np.cumsum(values)
# add 0 at front
cumulative = np.hstack((0,cumulative))
base = np.hstack((2*base[0]-base[1],base))
#plot the survival function
pl.plot(base[:-1], ( len(LNoDFS2rel)-cumulative ) * 100. / len(LNoDFS2rel), label="No DFS",color='c')
pl.legend(loc=0)
pl.savefig("../DFSStudy/PercentagePlotNorm.png")

