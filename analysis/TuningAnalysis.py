#! /usr/bin/env python

#  This file is part of PLACET
#
#  Copyright (C) 2015 Jochem Snuverink <jochem.snuverink@rhul.ac.uk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this library; see the file COPYING.LIB.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.

# In general, we want floating point division
from __future__ import division

import matplotlib as mpl
#mpl.use('Agg')
import pylab as pl
import os
import numpy as np
import sys as sys

from numpy import zeros

# Placet packages (add PLACET path PLACETDIR/scripts/placet/python to PYTHONPATH)
import BeamPlot

# default plotting
mpl.rc('axes', grid=True)
mpl.rc('legend',numpoints=1,fontsize=20)
mpl.rc('axes',labelsize=20)
mpl.rc('axes',titlesize=20)
mpl.rc('axes',linewidth=1.5)
mpl.rc('lines',linewidth=1.5) # line width in points

mpl.rc('xtick',labelsize=18) # fontsize of the tick labels
mpl.rc('ytick',labelsize=18) # fontsize of the tick labels
if sys.version[2]>"7":
    mpl.rc('xtick.major',width=1.5)
    mpl.rc('ytick.major',width=1.5)

#mpl.rc('font',family='monospace')
#mpl.rc('font',monospace='Andale Mono')
#mpl.rc('figure.subplot',left=0.2)
mpl.rc('figure.subplot',bottom=0.15)

def stringMeanStd(a):
    '''Helper Function that returns string with mean and standard deviation of array a'''    
    return str(round(a.mean(),1)) + ' (' + str(round(a.std(),1)) + ')'

def stringMean(a):
    '''Helper Function that returns string with mean of array a up to 1 decimal'''    
    return str(round(a.mean(),1))

class TuningStudySingle :
    '''
    Class for plotting the results from a single tuning seed

    Example
    Python {
    import TuningAnalysis
    TuningAnalysis.TuningStudySingle()
    }
   
    '''
    def __init__(self,directory="../output/", label="tuning", seed=-1, plots = False, positronLine = False, beamLine = False, beamPlot = False, knobPlot = True, convertLumi = True, savePlots = True, L0 = 5.9e34, outputFormat = "png") :
        '''
        Input options
        directory     : directory of the files to read
        label         : plotlabel
        plots         : make plots
        beamLine      : make beamline plots
        knobPlot      : make knob plots
        beamPlot      : make beam plots
        knobPlot      : make knob plots
        positronLine  : 1 beam (False, electron only) or 2 beams (True)
        convertLumi   : convert bunch luminosity into cm-2*s-1
        savePlots     : save plots
        outputFormat  : format of plots, starting with a dot
        '''
        self.dir             = os.getcwd() + "/" + directory + "/"
        print 'TuningStudy of:',self.dir
        self.label           = label
        self.seed            = seed
        self.plots           = plots
        self.positronLine    = positronLine
        self.beamLine        = beamLine
        self.beamPlot        = beamPlot
        self.knobPlot        = knobPlot
        self.convertLumi     = convertLumi
        self.savePlots       = savePlots
        self.outputFormat    = "." + outputFormat
        self.L0              = L0
        
        self._readFiles()

        if self.plots:
            self.plot()

    def plot(self) :

        self.controlPlot()

        if self.beamLine:
            self.beamlineStudy()
        if self.beamPlot:
            self.beamStudy()
        if self.knobPlot:
            self.knobStudy()
            
    def _readFiles(self) :
        if self.positronLine:
            self.Lumi = np.transpose(np.genfromtxt(self.dir + '/LumiTwo.dat'))
        else:
            self.Lumi = np.transpose(np.genfromtxt(self.dir + '/Lumi.dat'))
        
        if self.convertLumi:
            ####################################
            # CLIC conversion 312 * 50 / 10000 #
            ####################################
            self.Lumi = self.Lumi * 312*50/10000.
        self.emittH = np.transpose(np.genfromtxt(self.dir + '/emittH.dat'))
        self.emittV = np.transpose(np.genfromtxt(self.dir + '/emittV.dat'))
        self.sizeH  = 1000*np.transpose(np.genfromtxt(self.dir + '/sizeH.dat')) # convert to nm
        self.sizeV  = 1000*np.transpose(np.genfromtxt(self.dir + '/sizeV.dat')) # convert to nm

        # new file naming for correct.log
        fname = self.dir + '/correctelectron.log'
        if not os.path.isfile(fname):
            fname = self.dir + '/correct.log'
                
        self.correctElectron = np.genfromtxt(fname)
        #clean trailing zeros
        k = self.correctElectron[:,0]>0
        for i in range(1,8):
            #print np.size(k)
            #print np.size(self.correctElectron)
            k = self.correctElectron[:,i]>0 & k
        self.correctElectron = self.correctElectron[k]
        
        if self.positronLine:
            self.correctPositron = np.genfromtxt(self.dir + '/correctpositron.log')
            #clean trailing zeros
            k = self.correctPositron[:,0]>0
            for i in range(1,8):
                k = self.correctPositron[:,i]>0 & k
            self.correctPositron = self.correctPositron[k]

        self.correct = self.correctElectron

        if self.knobPlot:
            if self.positronLine:
                self.knobs = np.loadtxt(self.dir + '/knobs.dat',
                                        dtype={'names' : ('beamline', 'knob', 'knobnr', 'position' , 'luminosity'),
                                            'formats' : ('S8', 'S3', 'i1', 'f8', 'f8')})

            else:
                self.knobs = np.loadtxt(self.dir + '/knobs.dat',
                                        dtype={'names' : ('knob', 'knobnr', 'position' , 'luminosity'),
                                            'formats' : ('S3', 'i1', 'f8', 'f8')})
        
    def controlPlot(self) :
        plotNr = 1000

        # should be
        # labels = np.array(["start","121","DFS-1","Knobs-1","DFS-Hybrid","MultiAlign","Knobs-2"])
        # are at the moment - to be changed in script
        labels = np.array(["121","DFS-1","MultipolesOn","Knobs-1","DFS-Hybrid","MultiAlign","Knobs-2"])
        # which steps to use in plots
        index = np.array([True,True,False,True,True,False,True])
        labels = labels[index]
        if sys.version[2]>"7":
            nbins = np.count_nonzero(index)
        else:
            nbins = 5
        bins = range(0,nbins)
        
        pl.figure(plotNr + 1)
        pl.title('Luminosity after tuning steps')

        pl.xticks(bins,labels)
        pl.xlabel('stage')
        
        pl.ylabel('$L$ [cm$^{-2}$s$^{-1}$]')
        pl.plot(self.Lumi[index], label=self.label)
        if self.savePlots:
            pl.savefig(self.dir+"Lumi"+self.outputFormat)

        pl.figure(plotNr + 2)
        pl.title('Vertical Emittance after tuning steps')
        pl.xticks(bins,labels)
        pl.xlabel('stage')
        pl.ylabel('vert. emittance [nm]')
        pl.plot(self.emittV[index], label=self.label)
            
        if self.savePlots:
            pl.savefig(self.dir+"emittV"+self.outputFormat)

        pl.figure(plotNr + 3)
        pl.title('Horizontal emittance after tuning steps')
        pl.xticks(bins,labels)
        pl.xlabel('stage')
        pl.ylabel('hor. emittance [nm]')
        pl.plot(self.emittH[index], label=self.label)
        if self.savePlots:
            pl.savefig(self.dir+"emittH"+self.outputFormat)
        
        pl.figure(plotNr + 4)
        pl.title('Vertical size after tuning steps')
        pl.xticks(bins,labels)
        pl.xlabel('stage')
        pl.ylabel('$\sigma_x$ [nm]')
        pl.plot(self.sizeV[index], label=self.label)
        if self.savePlots:
            pl.savefig(self.dir+"sizeV"+self.outputFormat)

        pl.figure(plotNr + 5)
        pl.title('Horizontal size after tuning steps')
        pl.xticks(bins,labels)
        pl.xlabel('stage')
        pl.ylabel('$\sigma_y$ [nm]')
        pl.plot(self.sizeH[index], label=self.label)
        if self.savePlots:
            pl.savefig(self.dir+"sizeH"+self.outputFormat)

        # only plot when stage information (column 10 is present)
        if np.shape(self.correct)[1] > 9 and len(self.correct) > 0:

            # DFS1
            self.correct121 = self.correct[self.correct[:,9]==1]
            self.correctDFS1 = self.correct[self.correct[:,9]==2]
            self.correctDFS2 = self.correct[self.correct[:,9]==5]

            titles = ["121","DFS1","DFS2"]
            logs = [self.correct121,self.correctDFS1,self.correctDFS2]
            
            for i in range(0,3) :
                if len(logs[i]) < 1:
                    continue
          
                if (logs[i][:,0] > 0).all():
                    pl.figure(plotNr + 100*i + 6)
                    pl.title('$\chi^2$ ' + titles[i] + ' - Total')
                    pl.ylabel('$\chi^{2}$')
                    pl.xlabel('iteration step')
                    pl.semilogy(logs[i][:,0] , label=self.label)
                    if self.savePlots:
                        pl.savefig(self.dir+titles[i]+"Chi"+self.outputFormat)

                if (logs[i][:,1] > 0).all():
                    pl.figure(plotNr + 100*i + 7)
                    pl.title('$\chi^2$ ' + titles[i] + ' - Response')
                    pl.ylabel('$\chi^{2}$')
                    pl.xlabel('iteration step')
                    pl.semilogy(logs[i][:,1], label=self.label)
                    if self.savePlots:
                        pl.savefig(self.dir+titles[i]+"ChiR"+self.outputFormat)

                if (logs[i][:,2] > 0).all():
                    pl.figure(plotNr + 100*i + 8)
                    pl.title('$\chi^2$ ' + titles[i] + ' - Dispersion')
                    pl.ylabel('$\chi^{2}$')
                    pl.xlabel('iteration step')
                    pl.semilogy(logs[i][:,2], label=self.label)
                    if self.savePlots:
                        pl.savefig(self.dir+titles[i]+"ChiEta"+self.outputFormat)

                pl.figure(plotNr + 100*i + 11)
                pl.title('Emittance x ' + titles[i])
                pl.ylabel('Hor. Emittance [nm]')
                pl.xlabel('iteration step')
                pl.plot(logs[i][:,5], label=self.label)
                if self.savePlots:
                    pl.savefig(self.dir+titles[i]+"emittXStep"+self.outputFormat)
        
                pl.figure(plotNr + 100*i + 12)
                pl.title('Emittance y ' + titles[i])
                pl.ylabel('Vert. emittance [nm]')
                pl.xlabel('iteration step')
                pl.plot(logs[i][:,6], label=self.label)
                if self.savePlots:
                    pl.savefig(self.dir+titles[i]+"emittYStep"+self.outputFormat)

                pl.figure(plotNr + 100*i + 13)
                pl.title('Beam size x ' + titles[i])
                pl.ylabel('$\sigma_x$ [$\mu$m]')
                pl.xlabel('iteration step')
                pl.plot(logs[i][:,7], label=self.label)
                if self.savePlots:
                    pl.savefig(self.dir+titles[i]+"sizeXStep"+self.outputFormat)

                pl.figure(plotNr + 100*i + 14)
                pl.title('Beam size y ' + titles[i])
                pl.ylabel('$\sigma_y$ [$\mu$m]')
                pl.xlabel('iteration step')
                pl.plot(logs[i][:,8], label=self.label)
                if self.savePlots:
                    pl.savefig(self.dir+titles[i]+"sizeYStep"+self.outputFormat)
    
    def orbitStudy(self):
        self.orbitElec = OrbitStudy(directory=self.dir, title="electron")
        if self.positronLine:
            self.orbitPosi = OrbitStudy(directory=self.dir, title="positron", fileNameX="bpm_meas_x_posi_machine_1.dat", fileNameY="bpm_meas_y_posi_machine_1.dat", figureNumber=101)

    def beamlineStudy(self):
        #self.beamlineElec = BeamlineStudy(directory=self.dir, title="electron")
        #if self.positronLine:
        #    self.beamlinePosi = BeamlineStudy(directory=self.dir, title="positron", fileNameX="gm_x_posi_machine_1.dat", fileNameY="gm_y_posi_machine_1.dat", figureNumber=201)
        pass
        
    def beamStudy(self):
        self.beamElec = BeamPlot.BeamPlot(directory=self.dir, savePlots=self.savePlots)
        self.beamElec.controlPlot()
        self.beamElec.twoDimPlotHist()

        (x,xp,y,yp,E,stdE,stdx) = self.beamElec.analyseBeam(verbose=True)
        
        if self.positronLine:
            self.beamPosi = BeamPlot.BeamPlot(directory=self.dir,fileName="positron.ini", savePlots=True)

    def knobStudy(self):
        plotNr = 10000
        pl.figure(plotNr+1)

        if self.convertLumi:
            self.knobs[:]['luminosity'] = self.knobs[:]['luminosity'] * 312*50/10000.

        # normalise
        self.knobs[:]['luminosity'] = self.knobs[:]['luminosity'] / self.L0
        
        pl.plot(self.knobs[:]['luminosity'])
        pl.title('knobs every pulse')
        pl.xlabel('stage')
        pl.ylabel('$L/L_0$')
        if self.savePlots:
            pl.savefig(self.dir+"KnobsPulse"+self.outputFormat)

        nrKnobsSteps = np.size(self.knobs)
        self.knobsLumiStep = zeros(nrKnobsSteps)
        self.knobsPosStep  = zeros(nrKnobsSteps)
        currentKnob   = self.knobs[0]['knob'] # current knob number
        currentKnobNr = self.knobs[0]['knobnr'] # current knob number
        if self.positronLine:
            currentBeamline = self.knobs[0]['beamline']
        
        maxLumi = 0 # maximum lumi for this knob
        maxKnobPos = 0 # knob position for the maximum lumi
        i = 0 # knob step counter
        for k in range(0,nrKnobsSteps):
            # determine maximum lumi for this knob (not completely correct as a fit is done, but a reasonable number
            if self.positronLine:
                sameKnob = self.knobs[k]['knobnr']==currentKnobNr and self.knobs[k]['knob']==currentKnob and self.knobs[k]['beamline'] == currentBeamline
            else:
                sameKnob = self.knobs[k]['knobnr']==currentKnobNr and self.knobs[k]['knob']==currentKnob

            if sameKnob:
                if self.knobs[k]['luminosity'] > maxLumi:
                    maxLumi    = self.knobs[k]['luminosity']
                    maxKnobPos = self.knobs[k]['position']
            else:
                # on to next knob and save this one
                self.knobsLumiStep[i] = maxLumi
                self.knobsPosStep[i]  = maxKnobPos
                maxLumi = 0
                maxKnobPos = 0
                i+=1
                currentKnob     = self.knobs[k]['knob']
                currentKnobNr   = self.knobs[k]['knobnr']
                if self.positronLine:
                    currentBeamline = self.knobs[k]['beamline']
    
        # fill last knob step
        self.knobsLumiStep[i] = maxLumi
        self.knobsPosStep[i]  = maxKnobPos

        self.nrKnobs = i+1

        print 'nr of luminosity measurements ', nrKnobsSteps
        print 'nr of knob steps ', self.nrKnobs
        # reduce array
        k = self.knobsLumiStep > 0
        self.knobsLumiStep = self.knobsLumiStep[k]
        self.knobsPosStep = self.knobsPosStep[k]

        # improvement for each knob
        self.knobsLumiStepDiff = np.diff(self.knobsLumiStep)
                
        pl.figure(plotNr+2)
        pl.title('Maximum luminosity after each knob')
        pl.plot(self.knobsLumiStep)
        pl.xlabel('knob')
        pl.ylabel('$L/L_0$')
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnob"+self.outputFormat)

        pl.figure(plotNr+3)
        pl.title('Position change of each knob')
        pl.plot(self.knobsPosStep)
        pl.xlabel('knob')
        pl.ylabel('Knob Position [a.u.]')
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobPos"+self.outputFormat)

        pl.figure(plotNr+4)
        pl.title('Luminosity improvement after each knob')
        pl.plot(self.knobsLumiStepDiff)
        pl.xlabel('knob')
        pl.ylabel('$\Delta L/L_0$')
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobDiff"+self.outputFormat)
        
        pl.figure(plotNr+5)
        pl.plot(self.knobsPosStep[1:],self.knobsLumiStepDiff,'o')
        pl.xlabel('Knob Position [a.u.]')
        pl.ylabel('$\Delta L/L_0$')
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobPosvsLumiImp"+self.outputFormat)
                
class TuningStudy :
    '''
    Class for plotting the results from a list of tuning seeds

    Example
    Python {
    import TuningAnalysis
    TuningAnalysis.TuningStudy()
    }
   
    '''
    def __init__(self,directory="../output/",label="tuning", plots = True, savePlots = True, color='b', positronLine = False, beamLine = False, beamPlot = False, knobPlot = True, L0 = 5.9e34, sizeX0 = 44.61, sizeY0 = 0.94, outputFormat = "png") :
        '''
        Input options
        directory     : directory of the files to read
        label         : plotlabel
        plots         : make plots
        savePlots     : save plots
        color         : color
        positronLine  : 1 beam (False, electron only) or 2 beams (True)
        L0            : nominal Lumi
        sigmaX0       : nominal beamsize x
        sigmaY0       : nominal beamsize y
        outputFormat  : format of plots, starting with a dot
        '''
        self.dir             = directory + "/"
        self.label           = label
        self.plots           = plots
        self.savePlots       = savePlots
        self.color           = color
        self.positronLine    = positronLine
        self.beamLine        = beamLine
        self.beamPlot        = beamPlot
        self.knobPlot        = knobPlot
        self.L0              = L0
        self.sizeX0          = sizeX0
        self.sizeY0          = sizeY0
        self.outputFormat    = "." + outputFormat
        
        self.anaList  = []
        self.seedList = []
        self.size     = 0

    def Add(self,TuningStudySingle):
        self.anaList.append(TuningStudySingle)
        self.seedList.append(TuningStudySingle.seed)
        self.size += 1
        
    def Analyse(self):

        print 'nr of seeds analysed ', self.size
        if self.positronLine:
            self.nrBeams = 2*self.size
        else:
            self.nrBeams = self.size
        
        self.colors = ['b','r','g','c','m','y','k','burlywood','chartreuse','orange','skyblue']
        self.markers = ['o','*','p','v','s','D','H','+','1','2','3','4']

        if self.plots:
            self.tuningStudy()

        if self.beamLine:
            self.beamlineStudy()
        if self.beamPlot:
            self.beamStudy()
        if self.knobPlot:
            self.knobStudy()

    def tuningStudy(self):
        nrTuningSteps = len(self.anaList[0].Lumi)
        self.Lumi   = zeros((nrTuningSteps,self.size))
        self.emittH = zeros((nrTuningSteps,self.nrBeams))
        self.emittV = zeros((nrTuningSteps,self.nrBeams))
        self.sizeH  = zeros((nrTuningSteps,self.nrBeams))
        self.sizeV  = zeros((nrTuningSteps,self.nrBeams))
        self.tuningSteps = zeros(self.size)
        
        for k in range(0,self.size):
            TuningAna = self.anaList[k]
            self.Lumi[:,k] = TuningAna.Lumi
            self.LumiRel= self.Lumi / self.L0

            if self.positronLine:
                self.emittH[:,2*k:2*k+2] = TuningAna.emittH
                self.emittV[:,2*k:2*k+2] = TuningAna.emittV
                self.sizeH[:,2*k:2*k+2]  = TuningAna.sizeH
                self.sizeV[:,2*k:2*k+2]  = TuningAna.sizeV

            else:
                self.emittH[:,k] = TuningAna.emittH
                self.emittV[:,k] = TuningAna.emittV
                self.sizeH[:,k]  = TuningAna.sizeH
                self.sizeV[:,k]  = TuningAna.sizeV
                
            if TuningAna.knobPlot:
                self.tuningSteps[k] = max(np.shape(TuningAna.knobs))

        self.LumiEst = self.sizeX0 * self.sizeY0 / (self.sizeH * self.sizeV) * self.L0

        # TODO check labels        
        #self.labels = ["start","121","DFS-1","Knobs-1","DFS-2","MultiAlign","Knobs-2","2nd order"]
        self.labels = ["1-to-1","","DFS","Sextupoles Knobs","DFS2","DFS2","Knobs-2","2nd order"]

        if self.positronLine:
            self.twoBeamTuningPlots()

        self.tuningPlots()

    def tuningPlots(self):
                       
        plotNr = 10
        
        pl.figure(plotNr+1)
        pl.clf()
        for i in [3,6]:
            print i, self.Lumi[i,:]/self.L0
            pl.hist(self.Lumi[i,:],bins=10, label=self.labels[i],color=self.colors[i],histtype='step')

        pl.xlabel('$L$ [cm$^{-2}$s$^{-1}$]')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "LumiHist"+self.outputFormat)

        pl.figure(plotNr+2)
        pl.clf()
        for i in [3,6]:
            pl.hist(self.Lumi[i,:]/self.L0,bins=10, label=self.labels[i],color=self.colors[i],histtype='step')

        pl.xlabel('$L/L_0$')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "LumiHistNorm"+self.outputFormat)

        
        pl.figure(plotNr+3)
        pl.clf()
        for i in [0,1,2,3,4,6]:
            labelsize = '{:<12}'.format(self.labels[i]) + ' ' + '{:>8}'.format(stringMean(self.emittH[i,:]) + ' nm')
            pl.hist(self.emittH[i,:],bins=10,label=labelsize,color=self.colors[i],histtype='step')
            
        pl.xlabel('hor. emittance [nm]')
        pl.xlim([0,100])
        pl.legend(loc=0,prop={'size':12})
        if self.savePlots:
            pl.savefig(self.dir + "emittHHist"+self.outputFormat)

        pl.figure(plotNr+4)
        pl.clf()
        for i in [0,1,2,3,4,6]:
            labelsize = '{:<12}'.format(self.labels[i]) + ' ' + '{:>8}'.format(stringMean(self.emittV[i,:]) + ' nm')
            pl.hist(self.emittV[i,:],bins=10, label=labelsize,color=self.colors[i],histtype='step')

        pl.xlabel('vert. emittance [nm]')
        pl.xlim([0,30])
        pl.legend(loc=0,prop={'size':12})
        if self.savePlots:
            pl.savefig(self.dir + "emittVHist"+self.outputFormat)

        pl.figure(plotNr+5)
        pl.clf()
        for i in [0,1,2,3,4,6]:
            label = self.labels[i] + ' ' + stringMean(self.sizeH[i,:]) + ' nm'
            pl.hist(self.sizeH[i,:],bins=10, label=label,color=self.colors[i],histtype='step')

        pl.xlabel('$\sigma_x$ [nm]')
        pl.legend(loc=0,prop={'size':12})
        if self.savePlots:
            pl.savefig(self.dir + "sizeHHist"+self.outputFormat)

        pl.figure(plotNr+6)
        pl.clf()
        #for i in [0,1,2,3,4,6]:
        for i in [0,2,3]:
            label = self.labels[i] + ' ' + stringMean(self.sizeV[i,:]) + ' nm'
            pl.hist(self.sizeV[i,:],bins=10, label=label,color=self.colors[i],histtype='step')

        pl.xlabel('$\sigma_y$ [nm]')
        pl.legend(loc=0,prop={'size':12})
        if self.savePlots:
            pl.savefig(self.dir + "sizeVHist"+self.outputFormat)

        # Nr of lumi measurements
        pl.figure(plotNr+7)
       
        pl.hist(self.tuningSteps,bins=10,label=self.label,histtype='step')
       
        pl.xlabel('nr luminosity measurements')
        pl.legend(loc=0,prop={'size':12})
        if self.savePlots:
            pl.savefig(self.dir + "tuningSteps"+self.outputFormat)
            
        # Final beamsizes
        pl.figure(plotNr+8)
        pl.hist(self.sizeH[-1,:],bins=100, label=self.label + ', av. : ' + stringMean(self.sizeH[-1,:]) + ' nm',histtype='step')
        pl.xlabel('$\sigma_x$ [nm]')
        pl.legend(loc=0,prop={'size':12}) # reduce font size
        if self.savePlots:
            pl.savefig(self.dir + "sizeHHistFinal"+self.outputFormat)
      
        pl.figure(plotNr+9)
        pl.hist(self.sizeV[-1,:],bins=100, label=self.label + ', av. : ' + stringMean(self.sizeV[-1,:]) + ' nm',histtype='step')
        pl.xlabel('$\sigma_y$ [nm]')
        pl.legend(loc=0,prop={'size':12}) # reduce font size
        if self.savePlots:
            pl.savefig(self.dir + "sizeVHistFinal"+self.outputFormat)

        pl.figure(plotNr+10)
        pl.plot(self.sizeH[-1,:],self.sizeV[-1,:], self.color+'o', label=self.label)
        pl.xlabel('$\sigma_x$ [nm]')
        pl.ylabel('$\sigma_y$ [nm]')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "sizeHvsVFinal"+self.outputFormat)
        
        # estimated lumi from beamsize versus Lumi
        #pl.figure(plotNr+10)
        # TODOOO fix factor 2
        #pl.plot(2*self.LumiEst[-1,:],self.Lumi[-1,:],'o')
        #pl.xlabel("Estimated Lumi")
        #pl.ylabel("Lumi from Guinea-Pig")
        #if self.savePlots:
            #pl.savefig(self.dir + "LumiEst"+self.outputFormat)

        # Maximum Normalised Survival plot
        pl.figure(7)
        # Use max lumi from both knob steps
        self.maxLumi = np.maximum(self.Lumi[-1,:],self.Lumi[3,:])
        # Use lumi from last knob step
        #self.maxLumi = self.Lumi[-1,:]
        # Lumi relative to nominal lumi
        self.maxLumiRel = self.maxLumi / self.L0

        # lots of bins to make transition smooth
        values, base = np.histogram(self.maxLumiRel, bins=1000)
        #evaluate the cumulative
        cumulative = np.cumsum(values)
        # add 0 at front
        cumulative = np.hstack((0,cumulative))
        base = np.hstack((2*base[0]-base[1],base))
        #plot the survival function
        pl.plot(base[:-1], ( len(self.maxLumiRel)-cumulative ) * 100. / len(self.maxLumiRel), c=self.color,label=self.label)

        pl.xlabel('$L / L_0$')
        pl.ylabel('$\%$ of seed reaching $L/L_0$')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "maxPercentagePlotNorm"+self.outputFormat)

        # Normalised survival plot
        pl.figure(8)
        self.finalLumiRel = self.Lumi[-1,:] / self.L0
        # lots of bins to make transition smooth
        values, base = np.histogram(self.finalLumiRel, bins=1000)
        # evaluate the cumulative
        cumulative = np.cumsum(values)
        # add 0 at front
        cumulative = np.hstack((0,cumulative))
        base = np.hstack((2*base[0]-base[1],base))
        #plot the survival function
        pl.plot(base[:-1], ( len(self.finalLumiRel)-cumulative ) * 100. / len(self.finalLumiRel), c=self.color, label=self.label)

        pl.xlabel('$L / L_0$')
        pl.ylabel('$\%$ of seed reaching $L/L_0$')
        pl.legend(loc=0)
        pl.xlim(xmin=0)
        if self.savePlots:
            pl.savefig(self.dir + "percentagePlotNorm"+self.outputFormat)

        # Survival plot
        pl.figure(9)
        self.finalLumi = self.Lumi[-1,:]
        # lots of bins to make transition smooth
        values, base = np.histogram(self.finalLumi, bins=1000)
        # evaluate the cumulative
        cumulative = np.cumsum(values)
        # add 0 at front
        cumulative = np.hstack((0,cumulative))
        base = np.hstack((2*base[0]-base[1],base))
        #plot the survival function
        pl.plot(base[:-1], ( len(self.finalLumi)-cumulative ) * 100. / len(self.finalLumi), c=self.color,label=self.label)

        pl.xlabel('$L$ [cm$^{-2}$s$^{-1}$]')
        pl.ylabel('$\%$ of seed reaching $L$')
        pl.legend(loc=0)
        pl.xlim(xmin=0)
        if self.savePlots:
            pl.savefig(self.dir + "percentagePlot"+self.outputFormat)

        # final vs 2nd step lumi
        pl.figure(10)
        pl.plot(self.LumiRel[3,:],self.LumiRel[-1,:],self.color+'o',label=self.label)
        # change range so that x and y axis are same
        maxi = max(max(self.LumiRel[-1,:]),max(self.LumiRel[3,:]))
        pl.xlim([0,maxi*1.3])
        pl.ylim([0,maxi*1.3])
        # add line
        pl.plot([0,maxi*1.3],[0,maxi*1.3],'k:',lw=2)

        pl.legend(loc=0)
        pl.title('Rel. luminosity after sextupole knobs')
        pl.xlabel('$L / L_0$ first iteration')
        pl.ylabel('$L / L_0$ second iteration')
        if self.savePlots:
            pl.savefig(self.dir + "1stStepvs2nd"+self.outputFormat)

    def twoBeamTuningPlots(self):

        plotNr = 30

        pl.figure(plotNr)
        for i in [3,6]:
        #for i in range(0,7):
            emittH1 = self.emittH[i,::2] # even indices
            emittH2 = self.emittH[i,1::2] # odd indices
            pl.plot(emittH1, emittH2,self.markers[i], label=self.labels[i], color=self.colors[i])
            
        pl.xlabel('Hor. emittance beam 1 [nm]')
        pl.ylabel('Hor. emittance beam 2 [nm]')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "TwoBeamHorEmitt"+self.outputFormat)

        pl.figure(plotNr+1)
        for i in [3,6]:
        #for i in range(0,7):
            emittV1 = self.emittV[i,::2] # even indices
            emittV2 = self.emittV[i,1::2] # odd indices
            pl.plot(emittV1, emittV2,self.markers[i], label=self.labels[i], color=self.colors[i])

        pl.xlabel('Vert. emittance beam 1 [nm]')
        pl.ylabel('Vert. emittance beam 2 [nm]')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "TwoBeamVertEmitt"+self.outputFormat)

        pl.figure(plotNr+2)
        for i in [3,6]:
        #for i in range(0,7):
            sizeH1 = self.sizeH[i,::2] # even indices
            sizeH2 = self.sizeH[i,1::2] # odd indices
            pl.plot(sizeH1, sizeH2,self.markers[i], label=self.labels[i], color=self.colors[i])

        pl.xlabel('$\sigma_x$ beam 1 [nm]')
        pl.ylabel('$\sigma_x$ beam 2 [nm]')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "TwoBeamHorBeamsizeEmit"+self.outputFormat)

        pl.figure(plotNr+3)
        for i in [3,6]:
        #for i in range(0,7):
            sizeV1 = self.sizeV[i,::2] # even indices
            sizeV2 = self.sizeV[i,1::2] # odd indices
            pl.plot(sizeV1, sizeV2,self.markers[i], label=self.labels[i], color=self.colors[i])

        pl.xlabel('$\sigma_y$ beam 1 [nm]')
        pl.ylabel('$\sigma_y$ beam 2 [nm]')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "TwoBeamVertBeamsizeEmit"+self.outputFormat)
        
    def beamlineStudy(self):
        pass

    def beamStudy(self, default=None):
        '''
        histograms for beam study
        '''
        #if default:
            
        #for k in range(0,self.size):
        #    TuningAna = self.anaList[k]
        #    TuningAna.beamStudy()
        #    self.beamElec = BeamPlot.BeamPlot(directory=self.dir, savePlots=True)
        pass
            
    def knobStudy(self):

        # put all in one matrix, find maximum nr of knobs
        nrKnobsIters = 0
        for k in range(0,self.size):
            if nrKnobsIters < self.anaList[k].nrKnobs:
                nrKnobsIters = self.anaList[k].nrKnobs
        
        self.knobsLumi = zeros((nrKnobsIters,self.size))
        self.knobsPos  = zeros((nrKnobsIters,self.size))
        for k in range(0,self.size):
            TuningAna = self.anaList[k]
            nrSteps = TuningAna.nrKnobs                
            self.knobsLumi[:nrSteps,k] = TuningAna.knobsLumiStep
            self.knobsPos [:nrSteps,k] = TuningAna.knobsPosStep
            # add end lumi to rest
            self.knobsLumi[nrSteps:,k] = TuningAna.knobsLumiStep[-1]

        # todo: only take average for non-zeros somehow
        self.knobsAvLumi  = np.mean(self.knobsLumi,1)
        self.knobsStdLumi = np.std (self.knobsLumi,1)
        self.knobsAvPos   = np.mean(abs(self.knobsPos),1)
        self.knobsStdPos  = np.std (abs(self.knobsPos),1)

        # average improvement per knob
        self.knobsAvLumiDiff = np.diff(self.knobsAvLumi)

        # percentiles
        self.knobs90Lumi  = zeros((nrKnobsIters,1))
        self.knobs100Lumi = zeros((nrKnobsIters,1))
        self.knobs110Lumi = zeros((nrKnobsIters,1))

        for k in range(0,nrKnobsIters):
            self.knobs90Lumi[k]  = 100*np.count_nonzero(self.knobsLumi[k,:] > 0.9) / self.size
            self.knobs100Lumi[k] = 100*np.count_nonzero(self.knobsLumi[k,:] > 1.0) / self.size
            self.knobs110Lumi[k] = 110*np.count_nonzero(self.knobsLumi[k,:] > 1.1) / self.size
        
        # Stage analysis
        self.knobsAvLumiStage  = self.knobsAvLumi[(self.anaList[0].nrKnobs-1)::self.anaList[0].nrKnobs]
        self.knobsStdLumiStage = self.knobsStdLumi[(self.anaList[0].nrKnobs-1)::self.anaList[0].nrKnobs]
        plotNr = 10000 + 20
        # average luminosity after each knob
        pl.figure(plotNr)
        pl.title('Average Luminosity')
        pl.xlabel('knob number')
        pl.ylabel('$L/L_0$')
        #pl.plot(np.mean(self.knobsLumi,1), label=self.label)
        pl.errorbar(range(0,nrKnobsIters),self.knobsAvLumi,yerr=self.knobsStdLumi,label=self.label,linestyle='-',marker='o')
        pl.ylim([0,1.3])

        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobAv"+self.outputFormat)

        # average position change after each knob
        pl.figure(plotNr+1)
        pl.title('Average Knob Absolute Position Change')
        pl.xlabel('knob number')
        pl.ylabel('[a.u]')
        #pl.plot(np.mean(self.knobsLumi,1), label=self.label)
        pl.errorbar(range(0,nrKnobsIters),self.knobsAvPos,yerr=self.knobsStdPos,label=self.label,linestyle='-',marker='o')
        #pl.ylim([0,1.3])

        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobAvPos"+self.outputFormat)

        # percentage seeds > 90% lumi after each knob
        pl.figure(plotNr+2)
        pl.title('% of seeds > 90% $L/L_0$')
        pl.xlabel('knob number')
        pl.ylabel('% of seeds')
        pl.plot(self.knobs90Lumi,label=self.label,linestyle='-',marker='o')
        pl.ylim([0,130])
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobAv90"+self.outputFormat)
        
        # percentage seeds > 100% lumi after each knob
        pl.figure(plotNr+3)
        pl.title('% of seeds > 100% $L/L_0$')
        pl.xlabel('knob number')
        pl.ylabel('% of seeds')
        pl.plot(self.knobs100Lumi,label=self.label,linestyle='-',marker='o')
        pl.ylim([0,130])
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobAv100"+self.outputFormat)

        # percentage seeds > 110% lumi after each knob
        pl.figure(plotNr+4)
        pl.title('% of seeds > 110% $L/L_0$')
        pl.xlabel('knob number')
        pl.ylabel('% of seeds')
        pl.plot(self.knobs110Lumi,label=self.label,linestyle='-',marker='o')
        pl.ylim([0,130])
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir+"KnobsKnobAv110"+self.outputFormat)

        # average luminosity after each stage (10 knobs)
        pl.figure(plotNr+5)
        pl.title('Average Luminosity')
        pl.xlabel('stage')
        pl.ylabel('$L/L_0$')
        #pl.plot(self.knobsAvLumiStage], label=self.label)
        pl.errorbar(range(0,len(self.knobsAvLumiStage)),self.knobsAvLumiStage,yerr=self.knobsStdLumiStage,label=self.label,linestyle='-',marker='o')
        pl.xlim(xmin=-1) # to make points at 0 visible
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir+"KnobsStage"+self.outputFormat)

        # average improvement per knob
        pl.figure(plotNr+6)
        pl.title('Average Luminosity improvement')
        pl.xlabel('knob number')
        pl.ylabel('$\Delta L/L_0$')
        # no improvement info for first knob
        pl.plot(range(2,len(self.knobsAvLumiDiff)+2), self.knobsAvLumiDiff, label=self.label)
        ymin,ymax = pl.ylim()
        pl.ylim([0,max(max(self.knobsAvLumiDiff),ymax)])
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir+"KnobsImprovement"+self.outputFormat)

        # average improvement per knob, to be redone
        # reshape av lumi diff
        # add zero in front to make again correct shape(!)
        #dummy = np.insert(self.knobsAvLumiDiff,0,0)
        #self.knobsAvLumiDiffReshaped = np.reshape(dummy,(self.anaList[0].nrKnobs,self.anaList[0].nrKnobStages))

        #pl.figure(plotNr+7)
        #clean plot
        #pl.clf()
        #pl.imshow(self.knobsAvLumiDiff,interpolation='none')
        #fig, ax = pl.subplots()
        #pl.title('Av. Lumi improvement')
        #pl.xlabel('stage')
        #pl.ylabel('knob number')
        #pl.ylabel('$\Delta L$ [cm$^{-2}$s$^{-1}$]')
        #pl.ylim(ymin=0)
        #pl.colorbar()
        #if self.savePlots:
        #    pl.savefig(self.dir+"KnobsImprovement2"+self.outputFormat)
        
class TuningStudyArray :
    '''
    Class for plotting the results from a list of list of tuning seeds

    Example
    Python {
    import TuningAnalysis
    TuningAnalysis.TuningStudyArray()
    }
   
    '''
    def __init__(self,directory="../output/",savePlots = True, label='Maximum',outputFormat = "png") :
        '''
        Input options
        directory     : directory of the files to read
        outputFormat  : format of plots, starting with a dot
        '''
        self.dir             = directory + "/"
        self.savePlots       = savePlots
        self.label           = label
        self.outputFormat    = "." + outputFormat
        # check if directory exists and if not make it
        if not os.path.exists(directory):
            os.makedirs(directory)
                
        self.size = 0
        self.anaList = []
        
    def Add(self,TuningStudy):
        self.anaList.append(TuningStudy)
        self.size += 1

    def Analyse(self, maxPlot = False):

        if self.size == 0:
            pass

        # intersection method
        def intersect(a, b):
            return list(set(a) & set(b))
        
        # find list of seeds that are present in all
        self.seedList = self.anaList[0].seedList
        for i in range(1,self.size):
            TuningAna = self.anaList[i]
            self.seedList = intersect(self.seedList, TuningAna.seedList)

        self.commonLRel        = zeros((self.size,len(self.seedList)))
        self.commonTuningSteps = zeros((self.size,len(self.seedList)))
        self.sumTuningSteps    = zeros(len(self.seedList))
        
        for i in range(0,self.size):
            TuningAna = self.anaList[i]
            #indices, relies on seeds sorted!
            indices = np.searchsorted(TuningAna.seedList,self.seedList)
            self.commonLRel[i,:]        = TuningAna.finalLumiRel[indices]
            self.commonTuningSteps[i,:] = TuningAna.tuningSteps[indices]
            self.sumTuningSteps        += self.commonTuningSteps[i,:]
                    
        if maxPlot:
            # find maximum Lumi
            self.maxLumi = zeros((np.size(self.seedList)))
            for i in range(0,self.size):
                TuningAna = self.anaList[i]
            
                #indices, relies on seeds sorted!
                indices = np.searchsorted(TuningAna.seedList,self.seedList)
                lumi = TuningAna.maxLumi[indices]
                #print lumi
                self.maxLumi = np.maximum(self.maxLumi,lumi)
                            
            pl.figure(7)
            self.maxLumiRel = self.maxLumi / self.anaList[0].L0

            # lots of bins to make transition smooth
            values, base = np.histogram(self.maxLumiRel, bins=1000)
            #evaluate the cumulative
            cumulative = np.cumsum(values)
            # add 0 at front
            cumulative = np.hstack((0,cumulative))
            base = np.hstack((2*base[0]-base[1],base))
            #plot the survival function
            pl.plot(base[:-1], ( len(self.maxLumiRel)-cumulative ) * 100. / len(self.maxLumiRel), c='y',label=self.label)

            pl.xlabel('$L / L_0$')
            pl.ylabel('$\%$ of seed reaching $L/L_0$')
            pl.legend(loc=0)
            if self.savePlots:
                pl.savefig(self.dir + "maxIterPercentagePlotNorm"+self.outputFormat)

        # cumulative histogram that shows maximum at end of each iteration
        pl.figure(1337)
        # maxLumi after ith step
        self.maxLumi = zeros((np.size(self.seedList)))

        for i in range(0,self.size):
            
            TuningAna = self.anaList[i]
            #indices, relies on seeds sorted!
            indices = np.searchsorted(TuningAna.seedList,self.seedList)
            
            lumi = TuningAna.maxLumi[indices]
            # Max lumi after ith step
            self.maxLumi = np.maximum(self.maxLumi,lumi)
            self.maxLumiRel = self.maxLumi / self.anaList[0].L0

            # lots of bins to make transition smooth
            values, base = np.histogram(self.maxLumiRel, bins=1000)
            #evaluate the cumulative
            cumulative = np.cumsum(values)
            # add 0 at front
            cumulative = np.hstack((0,cumulative))
            base = np.hstack((2*base[0]-base[1],base))
            #plot the survival function
            pl.plot(base[:-1], ( len(self.maxLumiRel)-cumulative ) * 100. / len(self.maxLumiRel), c=TuningAna.colors[i],label=TuningAna.label)

        pl.xlabel('$L / L_0$')
        pl.ylabel('$\%$ of seed reaching $L/L_0$')
        pl.legend(loc=0)
        if self.savePlots:
            pl.savefig(self.dir + "maxatEndIterPercentagePlotNorm"+self.outputFormat)

        for i in range(0,self.size-1):
            pl.figure(1338 + i)
            L1 = self.commonLRel[i,:]
            L2 = self.commonLRel[i+1,:]
            pl.plot(L2,L1,'o',label=self.label)
            # change range so that x and y axis are same
            maxi = max(max(L2),max(L1))
            pl.xlim([0,maxi*1.3])
            pl.ylim([0,maxi*1.3])
            
            pl.title('Luminosity Comparison between iterations')
            pl.xlabel('$L / L_0$ ' + self.anaList[i+1].label)
            pl.ylabel('$L / L_0$ ' + self.anaList[i].label)
            # add line
            pl.plot([0,maxi*1.3],[0,maxi*1.3],'k:',lw=2)
            
            if self.savePlots:
                pl.savefig(self.dir + "iter" + str(i+1) + "vsiter" + str(i+2) + ""+self.outputFormat)

        # nr of cumulative luminosity measurements
        pl.figure(3000)
        pl.hist(self.sumTuningSteps, bins=10,label=self.label,histtype='step')
        if self.savePlots:
            pl.savefig(self.dir + "sumTuningSteps"+self.outputFormat)
