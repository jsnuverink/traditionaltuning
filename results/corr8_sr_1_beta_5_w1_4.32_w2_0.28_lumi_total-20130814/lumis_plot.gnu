set xlabel 'L/L_0'
set ylabel '% machines with L>x axis value'
#set xrange [1e32:5e34]
set xrange [0:1.15]
set yrange [0:110]
#set xtics 0.5e34
set xtics 0.1
set ytics 10
set grid
set key left
#L0=4.45e34
#L0=3.51232e+34
L0=1.23563e+34
#p 'lumi_perf.dat' u ($1/L0):($2*100) w lp t 'First pass'

set term postscript enhanced color
set output 'lumi_plot.ps'
p 'lumi_perf.dat' u ($1/L0):($2*100) w steps lw 3 t 'BBA+Knobs1+DFS+Knobs2, L_0=2.20·10^{34} cm^{-2}s^{-1}'
