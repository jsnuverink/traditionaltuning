from numpy import *
import os
import string
from math import *

os.system("grep luminosity machine_bpmres_0.010_* > lumis.dat")
file1=open('lumi_step_stat.dat','w')
# Sorting luminosities

ext='lumis.dat'

column = 8
L0=354*50/10000.0

for i in range(2,9):
	column = i
	l=0
	y_val=[]
	for line in open(ext):
    		if l>=1:
        		sline=line.split()
			y_val.append(float(sline[column]))
        		l+=1
    		else : l+=1
	x0=mean(y_val)
	dev=std(y_val)
	lumi_sort=sorted(y_val, reverse=True)

	print 'Step',column-1, ': <L> =', x0*L0, '+/-', dev*L0
	print>>file1, column-1, x0*L0, dev*L0


file1.close()
