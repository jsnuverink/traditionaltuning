from numpy import *
import os
import string
from math import *

os.system("grep luminosity machine_bpmres_0.010_* > lumis.dat")
file1=open('lumi_sort.dat','w')
file2=open('lumi_perf.dat','w')
# Sorting luminosities

ext='lumis.dat'
l=0
y_val=[]
y_val2=[]
for line in open(ext):
    if l>=1:
        sline=line.split()
	y_val.append(float(sline[8]))
	y_val2.append(float(sline[8])*float(sline[8]))
        l+=1
    else : l+=1
x0=sum(y_val)/len(y_val)
ave2=sqrt(sum(y_val2)/len(y_val2))
lumi_sort=sorted(y_val, reverse=True)
print len(y_val)
for i in range(len(y_val)):
	print >> file1, lumi_sort[i]

# Luminosity performance
#L_0=4.43351e34 #w/o synch
L_0=3.51232e34
counts=[]
for j in range(300):
	L=L_0*(1.5-0.005*j)
	#print L
	bin=0
	for i in range(len(y_val)):
		if lumi_sort[i]>=L:
			bin=bin+1.0
	print >> file2, L, bin/len(y_val)
file1.close()
file2.close()

os.system("gnuplot lumis_plot.gnu")
