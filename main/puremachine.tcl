# comment out get_lumi when log == 0
# comment out tracking of beam0t, when log == 0, except for last tracking

#############################################################################
#
# puremachine.tcl
#
#
#############################################################################

# Disable multi-core option 
#ParallelThreads -num 1 

# Set initial energy in GeV
set e_initial 1500
set e0 $e_initial

set script_dir /home/bodenstein/simulation/programs/placet/jochem/
if {![file exist $script_dir]} {
    set script_dir [pwd]/..
}

array set args {
    sr 1
    gain1 0.5
    gain2 0.2
    iter1 100
    iter2 40
    sigma 10.0
    bpmres 0.010
    deltae 0.001
    deltakx -1
    deltaky -1
    beta 11
    w1 73
    w2 5
    log 2
    machine 1
    loadmachine 0
    repeat 1
}

array set args $argv

set gain1 $args(gain1)
set gain2 $args(gain2)
set iter1 $args(iter1)
set iter2 $args(iter2)
set sigma $args(sigma)
set bpmres $args(bpmres)
set deltae $args(deltae)
set deltakx $args(deltakx)
set deltaky $args(deltaky)
set w1 $args(w1)
set w2 $args(w2)
set log $args(log)
set beta $args(beta)
set sr $args(sr)
set machine $args(machine)
set loadmachine $args(loadmachine)
set repeat $args(repeat)

if { $w2 == -1 } {
    set w2 $w1
}

# Scale is scaling the lattice depending on Energy (1500 = 1 is  default)
set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

#Turn on SR everywhere
set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

# Reference energy, then look at the lattice file which depends on energy
SetReferenceEnergy $e0
source $script_dir/lattices/trad_ffs_sext_0.7.tcl

#Add beam measurement at the end of the line
#This is overwriting, but not showing the things shown.
if { $log == 2 } {
    TclCall -script {
	array set bm [BeamMeasure]
	set beam_emitt_x $bm(emitt_x)
	set beam_emitt_y $bm(emitt_y)
	set beam_size_x [expr sqrt($bm(sxx) - $bm(x) * $bm(x))]
	set beam_size_y [expr sqrt($bm(syy) - $bm(y) * $bm(y))]
    }
}

#This isn't used at the moment, but could be useful later.
Bpm -name IP
Drift -name "D0" -length 3.5
#Drift -name "D0" -length 3.5026092
Bpm

#Placet command stating that the beamline is now finished, and its name is test
BeamlineSet -name test

#Set initial beam parameters for simulation
array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x  34.142
    beta_y  5.858
}


#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
#Charge per bunch
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
#-1 is uniform dist. (waterbag), +1 is gaussian energy spread (not particles)
set match(e_spread) -1.0

#Setting energy differences (for DFS)
set e0 $e_initial

#If using DFS, turn on other energies:
#set e2 [expr $e0 * (1.0 + $deltae)]
#set e1 [expr $e0 * (1.0 - $deltae)]

#puts "e1 = $e1"
#puts "e2 = $e2"

Octave {
printf("octave working\n")
}

#This commented-out section is for DFS and uses multiple energies.

#Simple one particle tracking, 1 slice, 1 particle/slice
#if { $log != 2 } {
#    set n_slice 1
#    set n 1
#    set n_total [expr $n_slice*$n]
#
#    make_beam_many beam0 $n_slice $n
#    exec echo "$e0 0 0 0 0 0" > particles.in
#    BeamRead -file particles.in -beam beam0
#
#    make_beam_many beam2 $n_slice $n
#    exec echo "$e2 0 0 0 0 0" > particles.in
#    BeamRead -file particles.in -beam beam2
#
#    make_beam_many beam1 $n_slice $n
#    exec echo "$e1 0 0 0 0 0" > particles.in
#    BeamRead -file particles.in -beam beam1
#} else { #Now 100 slices with 1000 particles each
#    set n_slice 100
#    set n 1000
#    set n_total [expr $n_slice*$n]
#
#    make_beam_many beam0 $n_slice $n
#    make_beam_many_energy beam2 $n_slice $n [expr 1. + $deltae ]
#    make_beam_many_energy beam1 $n_slice $n [expr 1. - $deltae ]
#}

#This assures that you are still running with a full beam for Lumi measurements, etc...
set n_slice 100
set n 1000
set n_total [expr $n_slice*$n]

make_beam_many beam0t $n_slice $n

# 1 is thin lens tracking, 0 is thick lens tracking
FirstOrder 1

#Logs after the whole procedure
Octave {
    % initialise storage arrays for the 7 tuning steps
    emittH   = zeros(1, 7);
    emittV   = zeros(1, 7);
    sizeH    = zeros(1, 7);
    sizeV    = zeros(1, 7);
    Lumi     = zeros(1, 7);
    LumiPeak = zeros(1, 7);
    IP = placet_get_name_number_list("test", "IP");
}

#Logs after every step
Octave {
    global Log;
    MaxSteps = 4 * ($iter1 + $iter2) + 4;
    Log.Chi     = zeros(MaxSteps, 1);
    Log.Chi_R   = zeros(MaxSteps, 1);
    Log.Chi_ETA = zeros(MaxSteps, 1);
    Log.Chi_CB1 = zeros(MaxSteps, 1);
    Log.Chi_CB2 = zeros(MaxSteps, 1);
    Log.EmittX  = zeros(MaxSteps, 1);
    Log.EmittY  = zeros(MaxSteps, 1);
    Log.SizeX   = zeros(MaxSteps, 1);
    Log.SizeY   = zeros(MaxSteps, 1);
    Log.Stage   = zeros(MaxSteps, 1);
    Log.Lumi    = zeros(MaxSteps, 1);
    Log.LumiPeak= zeros(MaxSteps, 1);
}

#Shows the machine/seed number
    puts "MACHINE: $machine"
    
    Octave {
	Log.index = 1;
    }

#Load in an existing machine, if desired. Useful if splitting simulation into several simulations or iterating upon previous results
    Octave {
	if $loadmachine
	if exist("machine_status_$machine.dat", "file")
	disp("reading machine status...");
	load 'machine_status_$machine.dat';
	placet_element_set_attributes("test", STATUS);
	else
	disp("ERROR: machine status file not found");
	exit(1)
	end
	end
    }

    puts "initial luminosity"

# load guinea-pig scripts
source $script_dir/main/multipole_knobs_new.tcl

#Placet tracking as is, no corrections or changes, beamline name, beam name, misalignments (None means no misalignments),1 = number of times run,0=first elemeent, IP is last element
#get_lumi executes Guinea Pig
    Octave {
	[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
	
	emittH(1,1) = E(end,2)
        emittV(1,1) = E(end,6)
	sizeH(1,1) = std(B(:,2))
	sizeV(1,1) = std(B(:,3))
	
	save -ascii E.dat E;

	L = get_lumi(B)
	Lumi(1,1) = L;
	LumiPeak(1,1) = str2num(Tcl_GetVar("lumi_peak"))
    }

Octave {
    printf("RESULT: sizex %g %g %g %g %g %g %g\n", sum(sizeH(:,1)), sum(sizeH(:,2)), sum(sizeH(:,3)), sum(sizeH(:,4)), sum(sizeH(:,5)), sum(sizeH(:,6)), sum(sizeH(:,7)));
    printf("RESULT: sizey %g %g %g %g %g %g %g\n", sum(sizeV(:,1)), sum(sizeV(:,2)), sum(sizeV(:,3)), sum(sizeV(:,4)), sum(sizeV(:,5)), sum(sizeV(:,6)), sum(sizeV(:,7)));
    printf("RESULT: luminosity %g %g %g %g %g %g %g\n", sum(Lumi(:,1)), sum(Lumi(:,2)), sum(Lumi(:,3)), sum(Lumi(:,4)), sum(Lumi(:,5)), sum(Lumi(:,6)), sum(Lumi(:,7)));
    save -ascii emittH.dat emittH;
    save -ascii emittV.dat emittV;
    save -ascii sizeH.dat sizeH;
    save -ascii sizeV.dat sizeV;
    save -ascii Lumi.dat Lumi;
    save -ascii LumiPeak.dat LumiPeak;
    file = fopen("beamsize.out", "w");
    fprintf(file, " %.15g %.15g %.15g %.15g\n", sum(sizeH(:,1)), sum(sizeV(:,1)), std(sizeH(:,1)), std(sizeV(:,1)));
    fclose(file);
}
