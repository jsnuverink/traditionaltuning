set e_initial 1500
set e0 $e_initial
set script_dir /afs/cern.ch/user/h/hgarciam/public/CLIC_3TeV_lattices/trad_reopt/tuning_betas_500_sext_0.7

array set args {
    offset 0.1
    sr 0
}

array set args $argv

set offset $args(offset)
set sr $args(sr)

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0

source $script_dir/lattices/trad_ffs_sext_0.7.tcl
Bpm -name "IP"

Bpm

BeamlineSet -name test

# Traditional FFS match

array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x  34.142
    beta_y  5.858
}

#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set e0 $e_initial
set n_slice 100
set n 1000
set n_total [expr $n_slice*$n]

make_beam_many beam0t $n_slice $n
puts "Feix iniciat"

#if { $sr } {
#  SetReferenceEnergy -beamline test -beam beam0t -survey Zero -iter 5
#}

source $script_dir/main/multipole_knobs_new.tcl
puts "Inici del calcul"
Octave {
  KBI = get_knobs_list();
  Knobs.I = [ KBI; KBI ];
  Knobs.L = { "x" ; "x" ; "x" ; "x" ; "x" ; "y" ; "y" ; "y" ; "y" ; "y"};
  Knobs.K = zeros(15,length(Knobs.I));
  [E,B] = placet_test_no_correction("test", "beam0t", "None");
  S = std(B(:,[ 1 2 3 5 6 ]));
  Cov0 = cov(B(:,[ 1 2 3 5 6 ]));
  sizeX = S(2)
  sizeY = S(3)
  get_lumi(B);
  Lumi_peak = Tcl_GetVar("lumi_peak")
  Lumi_total = Tcl_GetVar("lumi_total")
  for k = 1:length(Knobs.I)
    placet_element_set_attribute("test", Knobs.I(k), Knobs.L(k), +$offset);
    [E,B] = placet_test_no_correction("test", "beam0t", "None");
    Cov = cov(B(:,[ 1 2 3 5 6 ]));
    placet_element_set_attribute("test", Knobs.I(k), Knobs.L(k), -$offset);
    [E,B] = placet_test_no_correction("test", "beam0t", "None");
    Cov -= cov(B(:,[ 1 2 3 5 6 ]));
    Cov /= 2 * $offset
    index = 1;
    for i = 1:5
      for j = i:5
        Knobs.K(index++,k) = Cov(i,j)/(S(i)*S(j));
      end
    end
    placet_element_set_attribute("test", Knobs.I(k), Knobs.L(k), 0.0);
  end
  
  Knobs.BeamStd = S;
  Knobs.BeamCov = Cov0;
  
  [U,S,V] = svd(Knobs.K);
  Knobs.S = diag(S);
  Knobs.V = V;

  Knobs.Ix = KBI;
  [Ux,Sx,Vx] = svd(Knobs.K(:,1:5));
  Knobs.Sx = diag(Sx);
  Knobs.Vx = Vx;

  Knobs.Iy = KBI;
  [Uy,Sy,Vy] = svd(Knobs.K(:,6:10));
  Knobs.Sy = diag(Sy);
  Knobs.Vy = Vy;

  N = rows(Knobs.K);
  if S(end) / S(1) / N < eps
    warning("some singular values might be too small!")
    disp(diag(S));
  end
}

Octave {
  save -text Knobs_sr_${sr}.dat Knobs
}
