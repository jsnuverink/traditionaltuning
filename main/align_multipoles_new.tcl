proc align_multipoles {} {
    global bpmres sigma repeat
    Octave {
	BI = placet_get_number_list("test", "bpm");
	placet_element_set_attribute("test", BI, "resolution", 0.0);
    }
    Octave {
        # ms = placet_element_get_attribute("test", MI, "strength");
        pinv_Sxx = pinv(Response0.Sxx);
        pinv_Syy = pinv(Response0.Syy);
        scale = 1;
        # placet_element_set_attribute("test", MI, "strength", 0.0);
        placet_test_no_correction("test", "beam0", "None");
        bpm0 = placet_get_bpm_readings("test");
        for multipole = [ 1:length(MI) (length(MI)-1):-1:1 ]
            multipole_type = placet_element_get_attribute("test", MI(multipole), "type");
            placet_element_set_attribute("test", MI(multipole), "strength", scale * MS(multipole));
            if multipole_type == 3
                sextupole = MI(multipole);
                if abs(placet_element_get_attribute("test", sextupole, "strength")) > 0
                    xs = placet_element_get_attribute("test", sextupole, "x");
                    ys = placet_element_get_attribute("test", sextupole, "y");
                    mode = 1 ; # 0 == along Y axis, 1 == along X axis, 2 == mixed
                    if mode == 0
                        placet_element_set_attribute("test", sextupole, "y", ys + MRANGEY(multipole));
                        # scan around the center (sometimes unstable, when the magnet is very offset)
                        # DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type-1),1,$repeat);
                        if xs > 0 ; # then it goes "left"
                            DX = repmat(linspace(-2 * MRANGEX(multipole), 0, multipole_type-1),1,$repeat);
                        else ; # else it goes "right"
                            DX = repmat(linspace(0,  2 * MRANGEX(multipole), multipole_type-1),1,$repeat);
                        end
                        data = zeros(size(DX));
                        index = 1;
                        for dx = DX
                            placet_element_set_attribute("test", sextupole, "x", xs + dx);
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_y = pinv_Syy * dbpm(:,2);
                            data(index) += theta_y(multipole);
                            index++;
                        end
                        xoffset = roots(polyfit(DX, data, 1));
                        placet_element_set_attribute("test", sextupole, "x", xs + xoffset + MRANGEX(multipole));
                        # scan around the center (sometimes unstable, when the magnet is very offset)
                        # DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type-1),1,$repeat);
                        if ys > 0 ; # then it goes "left"
                            DY = repmat(linspace(-2 * MRANGEY(multipole), 0, multipole_type-1),1,$repeat);
                        else ; # else it goes "right"
                            DY = repmat(linspace(0, 2 * MRANGEY(multipole), multipole_type-1),1,$repeat);
                        end
                        data = zeros(size(DY));
                        index = 1;
                        for dy = DY
                            placet_element_set_attribute("test", sextupole, "y", ys + dy);
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_y = pinv_Syy * dbpm(:,2);
                            data(index) += theta_y(multipole);
                            index++;
                        end
                        yoffset = roots(polyfit(DY, data, 1));
                        placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
                        placet_element_set_attribute("test", sextupole, "y", ys + yoffset);
                    elseif mode == 1
                        # scan around the center (sometimes unstable, when the magnet is very offset)
                        # DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
                        if xs > 0 ; # then it goes "left"
                            DX = repmat(linspace(-2 * MRANGEX(multipole), 0, multipole_type),1,$repeat);
                        else ; # else it goes "right"
                            DX = repmat(linspace(0,  2 * MRANGEX(multipole), multipole_type),1,$repeat);
                        end
                        data = zeros(size(DX));
                        index = 1;
                        for dx = DX
                            placet_element_set_attribute("test", sextupole, "x", xs + dx);			
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_x = pinv_Sxx * dbpm(:,1);
                            data(index) += theta_x(multipole);
                            index++;
                        end
                        xoffset = roots(polyderiv(polyfit(DX, data, 2)));
                        placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
                        # scan around the center (sometimes unstable, when the magnet is very offset)
                        # DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type),1,$repeat);
                        if ys > 0 ; # then it goes "left"
                            DY = repmat(linspace(-2 * MRANGEY(multipole), 0, multipole_type),1,$repeat);
                        else ; # else it goes "right"
                            DY = repmat(linspace(0, 2 * MRANGEY(multipole), multipole_type),1,$repeat);
                        end
                        data = zeros(size(DY));
                        index = 1;
                        for dy = DY
                            placet_element_set_attribute("test", sextupole, "y", ys + dy);
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_x = pinv_Sxx * dbpm(:,1);
                            data(index) += theta_x(multipole);
                            index++;
                        end
                        yoffset = roots(polyderiv(polyfit(DY, data, 2)));
                        placet_element_set_attribute("test", sextupole, "y", ys + yoffset);						
                    else # mode == 2
                        # scan around the center (sometimes unstable, when the magnet is very offset)
                        # DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
                        if xs > 0 ; # then it goes "left"
                            DX = repmat(linspace(-2 * MRANGEX(multipole), 0, multipole_type),1,$repeat);
                        else ; # else it goes "right"
                            DX = repmat(linspace(0,  2 * MRANGEX(multipole), multipole_type),1,$repeat);
                        end
                        data = zeros(size(DX));
                        index = 1;
                        for dx = DX
                            placet_element_set_attribute("test", sextupole, "x", xs + dx);			
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_x = pinv_Sxx * dbpm(:,1);
                            data(index) += theta_x(multipole);
                            index++;
                        end
                        xoffset = roots(polyderiv(polyfit(DX, data, 2)));
                        placet_element_set_attribute("test", sextupole, "x", xs + xoffset + MRANGEX(multipole));
                        # scan around the center (sometimes unstable, when the magnet is very offset)
                        # DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type-1),1,$repeat);
                        if ys > 0 ; # then it goes "left"
                            DY = repmat(linspace(-2 * MRANGEY(multipole), 0, multipole_type-1),1,$repeat);
                        else ; # else it goes "right"
                            DY = repmat(linspace(0, 2 * MRANGEY(multipole), multipole_type-1),1,$repeat);
                        end
                        data = zeros(size(DY));
                        index = 1;
                        for dy = DY
                            placet_element_set_attribute("test", sextupole, "y", ys + dy);
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_y = pinv_Syy * dbpm(:,2);
                            data(index) += theta_y(multipole);
                            index++;
                        end
                        yoffset = roots(polyfit(DY, data, 1));
                        placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
                        placet_element_set_attribute("test", sextupole, "y", ys + yoffset);
                    end
                end
            elseif multipole_type == 4 || multipole_type == 5
                xxxxpole = MI(multipole);
                if abs(placet_element_get_attribute("test", xxxxpole, "strength")) > 0
                    xs = placet_element_get_attribute("test", xxxxpole, "x");
                    ys = placet_element_get_attribute("test", xxxxpole, "y");
                    # scan around the center (sometimes unstable, when the magnet is very offset)
                    # DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
                    if xs > 0 ; # then it goes "left"
                        DX = repmat(linspace(-2 * MRANGEX(multipole), 0, multipole_type),1,$repeat);
                    else ; # else it goes "right"
                        DX = repmat(linspace(0,  2 * MRANGEX(multipole), multipole_type),1,$repeat);
                    end
                    data = zeros(size(DX));
                    index = 1;
                    for dx = DX
                        placet_element_set_attribute("test", xxxxpole, "x", xs + dx);
                        placet_test_no_correction("test", "beam0", "None");
                        bpm2 = placet_get_bpm_readings("test");
                        dbpm = bpm2 - bpm0;
                        theta_x = pinv_Sxx * dbpm(:,1);
                        data(index) += theta_x(multipole);
                        index++;
                    end
                    POL = polyfit(DX, data, multipole_type-1);
                    for deriv = 1:(multipole_type-2)
                        POL = polyderiv(POL);
                    end
                    xoffset = roots(POL);
                    placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset);
                    # scan around the center (sometimes unstable, when the magnet is very offset)
                    # DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type),1,$repeat);
                    if ys > 0 ; # then it goes "left"
                        DY = repmat(linspace(-2 * MRANGEY(multipole), 0, multipole_type),1,$repeat);
                    else ; # else it goes "right"
                        DY = repmat(linspace(0, 2 * MRANGEY(multipole), multipole_type),1,$repeat);
                    end
                    if multipole_type == 4
                        data = zeros(size(DY));
                        index = 1;
                        for dy = DY
                            placet_element_set_attribute("test", xxxxpole, "y", ys + dy);
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_y = pinv_Syy * dbpm(:,2);
                            data(index) += theta_y(multipole);
                            index++;
                        end
                        yoffset = roots(polyderiv(polyderiv(polyfit(DY, data, 3))));
                        placet_element_set_attribute("test", xxxxpole, "y", ys + yoffset);
                    else
                        placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset + MRANGEX(multipole));
                        data = zeros(size(DY));
                        index = 1;
                        for dy = DY
                            placet_element_set_attribute("test", xxxxpole, "y", ys + dy);
                            placet_test_no_correction("test", "beam0", "None");
                            bpm2 = placet_get_bpm_readings("test");
                            dbpm = bpm2 - bpm0;
                            theta_y = pinv_Syy * dbpm(:,2);
                            data(index) += theta_y(multipole);
                            index++;
                        end
                        yoffset = roots(polyderiv(polyderiv(polyfit(DY, data, 3))));
                        placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset);					
                        placet_element_set_attribute("test", xxxxpole, "y", ys + yoffset);
                    end		
                end
            end
            # placet_element_set_attribute("test", MI(multipole), "strength", 0.0);
        end
        # placet_element_set_attribute("test", MI, "strength", ms);
    }
    Octave {
	placet_element_set_attribute("test", BI, "resolution", $bpmres);
    }
}
