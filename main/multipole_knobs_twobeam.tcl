array set gp_param "
    energy 1500.0
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 400.0
    cut_y 50.0
    n_x 64
    n_y 256
    do_coherent 1
    n_t 1
    charge_sign -1.0"

source $script_dir/scripts/clic_guinea.tcl

proc run_guinea {off angle} {
    global gp_param
    # centre each beam , for 2 beams this is needed!
    centre_each
    set res [exec grid]
    set yoff [expr -0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr -0.5*([lindex $res 0]+[lindex $res 1])] 
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
        set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
    }
    if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
        set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
    }
    #puts "yoff $yoff"
    #puts "xoff $xoff"
    write_guinea_correct $xoff $yoff 0 0 0

    # find right guinea-pig, if on afs use that else local one!
    set guinea_exec /afs/cern.ch/eng/sl/clic-code/lx64slc5/guinea-pig/bin/guinea-old
    if { ![ file exist $guinea_exec] } {
	set guinea_exec guinea
    }
    exec $guinea_exec default_clic default result.out

    set gp_param(cut_x) $tx
    set gp_param(cut_y) $ty
    return [get_results result.out]
}

Octave {
  function L = get_lumi(B1,B2)
    if nargin==0
      IP1 = placet_get_name_number_list("electron", "IP");
      [E1,B1] = placet_test_no_correction("electron", "beam0_electron", "None", 1, 0, IP1);
      IP2 = placet_get_name_number_list("positron", "IP");
      [E2,B2] = placet_test_no_correction("positron", "beam0_positron", "None", 1, 0, IP2);
    end
    if nargin==1
      B2=B1;
    end
    save_beam("electron.ini", B1);
    save_beam("positron.ini", B2);
    
    Tcl_Eval("set res [run_guinea 0.0 0.0]");
    Tcl_Eval("set lumi_total [lindex \$res 0]");
    Tcl_Eval("set lumi_peak [lindex \$res 1]");
    L = str2num(Tcl_GetVar("lumi_total"));
  end
}

proc get_lumi_2 {electron,positron} {
    exec cp $electron "electron.ini"
    exec cp $positron "positron.ini"
    set res [run_guinea 0.0 0.0]
    set lumi_total [lindex \$res 0]
    set lumi_peak [lindex \$res 1]

    puts "lumi_total $lumi_total"
    puts "lumi_peak $lumi_peak"
}

Octave {
  function KBI = get_knobs_list()
    N = 5;
    NAME = [ "VS12"; "VS21"; "VS22"; "HS21"; "HS22" ];
    KBI = zeros(N, 1);
    k=1
    for i = 1:N
      for j = 1:2
         KBI(k) = placet_get_name_number_list(beamlinename(j,:), NAME(i,:));
         k++;
      end
    end
  end
}

Octave {
    global Knobs;
    load '$script_dir/matrices/Knobs_sr_${sr}.dat';
    global DFSKnobs;
    load '$script_dir/matrices/DFSKnobs.dat'
}

foreach axis { x y } {
  foreach knob { 1 2 3 4 5 } {
      foreach beam {electron positron} {
	  Octave {
	      function vary_knob${beam}${knob}${axis}(x)
	      global Knobs;
	      knobs = x * Knobs.V${axis}(:,$knob);
	      placet_element_vary_attribute("$beam", Knobs.I${axis}, "$axis", knobs);
	      end
	      
	      function L = test_knob${beam}${knob}${axis}(x)
	      vary_knob${beam}${knob}${axis}(+x);
	      L = -get_lumi();
	      vary_knob${beam}${knob}${axis}(-x);
	      printf("test_knob${beam}${knob}${axis}(%g) =  %g\n", x, -L);
	      knobfile = fopen("knobs.dat","a");
	      fprintf(knobfile,"${beam} ${axis} ${knob} %g %g\n", x, -L);
	      fclose(knobfile);
	      end
	  }
      }
  }
}

# DFS Knobs
for {set knob 1 } { $knob <= 10 } { incr knob } {
    foreach beam {electron positron} {
	Octave {
	    function vary_knob${beam}DFS${knob}(x)
	    global DFSKnobs;
	    global DI;
	    knobs = x * DFSKnobs.V(:,$knob);
	    M = length(DI);
	    placet_element_vary_attribute("$beam", DI, "strength_x", +knobs(1:M));
	    placet_element_vary_attribute("$beam", DI, "strength_y", +knobs((M+1):end));
	    end
	    function L = test_knob${beam}DFS${knob}(x)
	    vary_knob${beam}DFS${knob}(+x);
	    L = -get_lumi();
	    vary_knob${beam}DFS${knob}(-x);
	    printf("test_knob${beam}DFS${knob}(%g) =  %g\n", x, -L);
	    knobfile = fopen("knobs.dat","a");
	    fprintf(knobfile,"${beam} DFS ${knob} %g %g\n", x, -L);
	    fclose(knobfile);
	    end
	}
  }
}


Octave {
  function [x_min, f_min] = fmin_inverse_parabola(func, a, b, c)
    X = [ a b c ];
    F = [ feval(func, a) feval(func, b) feval(func, c) ];
    [f_min, i_min] = min(F);
    x_min = X(i_min);
    # hold on;
    for iter = 1:10
      P = polyfit(X, F, 2);
      # _X = linspace(min(X), max(X), 80);
      # _F = polyval(P, _X);
      # if P(1) > 0 
      #   plot(X,-F,'*',_X,-_F);
      # end
      # drawnow;
      if P(1) == 0
        disp("flat function!");
        break
      end
      while P(1) < 0
        disp("bracketing");
        [F, I] = sort(F);
        X = X(I);
        X(3) = X(1) + 1.6180 * (X(1) - X(3));
        F(3) = feval(func, X(3));
        P = polyfit(X, F, 2);
      end
      x_vertex = -P(2) / P(1) / 2;
      f_vertex = feval(func, x_vertex);
      X = [ X x_vertex ];
      F = [ F f_vertex ];	
      [F, I] = sort(F);
      X = X(I(1:3));
      F = F(1:3);
      x_min = X(1);
      f_min = F(1);
      X(3) = X(1) + 0.38197 * sign(X(1) - X(2)) * abs(X(1) - X(3));
      F(3) = feval(func, X(3));
    end
  end
}

#     foreach knob { 1 2 3 4 5 6 7 8 9 10 } { 
#    foreach knob { 10 9 8 7 6 5 4 3 2 1 } {

proc dfs_knobs { range } {
  foreach iter { 1 2 } {
      foreach knob { DFS1 DFS2 DFS3 DFS4 } {  
	  foreach beam {electron positron} {
	      Octave {
		  x = fmin_inverse_parabola("test_knob${beam}${knob}", -$range, 0.0, $range);
		  vary_knob${beam}${knob}(x);
	      }
	  }
    }
    set range [expr $range / 10.0]
  }
}

proc multipole_knobs { range } {
    foreach iter { 1 2 } {
	foreach axis {x y} {
	    foreach knob { 1 2 3 4 5 } {
		foreach beam {electron positron} {
		    Octave {
			x = fmin_inverse_parabola("test_knob${beam}${knob}${axis}", -$range, 0.0, $range);
			vary_knob${beam}${knob}${axis}(x);
		    }
		}
	    }
	}
        set range [expr $range / 10.0]
    }
}

proc th_knobs {nknobs} {
    Octave {
	Cov_diff = cov(B(:,[ 1 2 3 5 6 7 8 ])) - Knobs.BeamCov;
	T_Cov = zeros(15,1);
	index=1;
	for i = 1:5
	for j = i:5
	T_Cov(index++) = Cov_diff(i,j) / (Knobs.BeamStd(i)*Knobs.BeamStd(j));
	end
	end
	Knobs.BeamStd;
	%T_Knobs = -Knobs.K \ T_Cov
	T_Knobs = -pinv(Knobs.K, Knobs.S($nknobs)) * T_Cov
	placet_element_vary_attribute("electron", Knobs.I, Knobs.L, T_Knobs);                  
    }
}
