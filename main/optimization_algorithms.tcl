# Tracking

proc track {index} {
global machine log

	Octave {

		if $log == 2
			[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
		else
			[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
		end
		emittH($machine,$index) = E(end,2);
		emittV($machine,$index) = E(end,6);
		sizeH($machine,$index) = std(B(:,2));
		sizeV($machine,$index) = std(B(:,3));
		Lumi($machine,$index) = get_lumi(B);
		}
}

proc track_nolumi {index} {
global machine log

	Octave {

		if $log == 2
			[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
		else
			[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
		end
		emittH($machine,$index) = E(end,2);
		emittV($machine,$index) = E(end,6);
		sizeH($machine,$index) = std(B(:,2))
		sizeV($machine,$index) = std(B(:,3))
		}
}

# Multipoles on/off

proc mult_off {} {

Octave {
	disp("Multipoles off")
        placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));
	}
}

proc mult_on {} {
Octave {
	disp("Multipoles on")
        placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));
		}
}

# One to one correction

proc one_to_one {} {
puts "One to One"
global betax betay gain1 iter1

Octave {
    R0 = [ 
      Response0.Rxx Zero
      Zero Response0.Ryy
    ];

    I0x = [
      diag(ones(1,M)) zeros(M)
    ];

    I0y = [
      zeros(M) diag(ones(1,M))
    ];

    I0 = [     
      diag(ones(1,M)) zeros(M)
      zeros(M) diag(ones(1,M))
    ];

    i0 = zeros(2*M,1);
    	}

  Octave {
	# 1:1 correction
    	placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));
		K0 = [ R0 ; $betax * I0x; $betay * I0y ];

		for i=1:$iter1
			# tracking
 			placet_test_no_correction("test", "beam0", "None");
			# BPM reading
			Measure0.R = placet_get_bpm_readings("test");
			# Differential orbit
			b0 = [
				Measure0.R(:,1) - Response0.Model_R(:,1)
				Measure0.R(:,2) - Response0.Model_R(:,2)
			];
			# Calculating correction based on model
			k0 = -[ R0 ; $betax * I0x; $betay * I0y ] \ [ b0 ; i0 ];
			# Apply correction
			placet_element_vary_attribute("test", DI, "strength_x", $gain1 * k0(1:M));
			placet_element_vary_attribute("test", DI, "strength_y", $gain1 * k0((M+1):end));
			# Chi test
			#if $log
			chi1 = sum(b0 .** 2)+$betax**2*sum(k0(1:M).**2)+$betay**2*sum(k0((M+1):end).**2)
			#end
		end

                placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));
		}
 	
        # end of 1:1
	puts "End of One to One"
}


# DFS

proc dispersion_free_steering {weight} {
global beta log deltae gain2 iter2

	Octave { 
	disp("Start of DFS")
	# DFS (if weight != 0.)
		if $weight != 0

			A0 = [ R0 ; $weight * ETA0 ; $beta * I0 ];

			for i=1:$iter2
				placet_test_no_correction("test", "beam0", "None");

				if $log == 2
					Log.EmittX(Log.index) += str2num(Tcl_GetVar("beam_emitt_x"));
					Log.EmittY(Log.index) += str2num(Tcl_GetVar("beam_emitt_y"));
					Log.SizeX(Log.index) += str2num(Tcl_GetVar("beam_size_x"));
					Log.SizeY(Log.index) += str2num(Tcl_GetVar("beam_size_y"));
				end

				Measure0.R = placet_get_bpm_readings("test");
				Measure0.Eta = MeasureDispersion("test", "beam1", "beam2", $deltae);

				b0 = [
					Measure0.R(:,1) - Response0.Model_R(:,1)
					Measure0.R(:,2) - Response0.Model_R(:,2)
				];

				eta0 = [
					Measure0.Eta(:,1) - Response0.Model_Eta(:,1)
					Measure0.Eta(:,2) - Response0.Model_Eta(:,2)
				];

				y0 =  [ b0 ; $weight * eta0 ; $beta * i0 ];
				k0 = -[ R0 ; $weight * ETA0 ; $beta * I0 ] \ y0;

				placet_element_vary_attribute("test", DI, "strength_x", $gain2 * k0(1:M));
				placet_element_vary_attribute("test", DI, "strength_y", $gain2 * k0((M+1):end));

				if $log				
					chi2 = sum(y0 .** 2)
					chi_R = sum(b0 .** 2);
					chi_ETA = sum(eta0 .** 2);

					Log.Chi(Log.index) += chi2;
					Log.Chi_R(Log.index) += chi_R;
					Log.Chi_ETA(Log.index++) += chi_ETA;
				end
			end
		end
        disp("End of DFS")
	}


}
