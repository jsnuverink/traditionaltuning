# comment out get_lumi when log == 0
# comment out tracking of beam0t, when log == 0, except for last tracking

#############################################################################
#
# correct8.tcl
#
#############################################################################

# Disable multi-core option 
#ParallelThreads -num 1 

set e_initial 1500
set e0 $e_initial

set script_dir /afs/cern.ch/user/j/jsnuveri/work/traditional/tuning_betas_500_sext_0.7/
if {![file exist $script_dir]} {
    set script_dir [pwd]/..
}

array set args {
    sr 1
    gain1 0.5
    gain2 0.2
    iter1 100
    iter2 40
    sigma 10.0
    bpmres 0.010
    deltae 0.001
    deltakx -1
    deltaky -1
    beta 11
    w1 73
    w2 5
    log 2
    machine 1
    loadmachine 0
    repeat 1
}

array set args $argv

set gain1 $args(gain1)
set gain2 $args(gain2)
set iter1 $args(iter1)
set iter2 $args(iter2)
set sigma $args(sigma)
set bpmres $args(bpmres)
set deltae $args(deltae)
set deltakx $args(deltakx)
set deltaky $args(deltaky)
set w1 $args(w1)
set w2 $args(w2)
set log $args(log)
set beta $args(beta)
set sr $args(sr)
set machine $args(machine)
set loadmachine $args(loadmachine)
set repeat $args(repeat)

if { $w2 == -1 } {
    set w2 $w1
}

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0

set beamline_list [list electron positron]
Octave {
    global beamlinename = ['electron' ; 'positron'];
    global electron = 1
    global positron = 2
}
foreach name $beamline_list {
    BeamlineNew
    source $script_dir/lattices/trad_ffs_sext_0.7.tcl

    if { $log == 2 } {
	TclCall -script {
	    array set bm [BeamMeasure]
	    set beam_emitt_x $bm(emitt_x)
	    set beam_emitt_y $bm(emitt_y)
	    set beam_size_x [expr sqrt($bm(sxx) - $bm(x) * $bm(x))]
	    set beam_size_y [expr sqrt($bm(syy) - $bm(y) * $bm(y))]
	}
    }

    Bpm -name IP
    Drift -name "D0" -length 3.5
    #Drift -name "D0" -length 3.5026092
    Bpm
    BeamlineSet -name $name

    array set match {
	alpha_x 0.00
	alpha_y 0.00
	beta_x  34.142
	beta_y  5.858
    }
    
    #emittances unit is e-7
    set match(emitt_x)  6.6 
    set match(emitt_y)  0.2
    set match(charge) 3.72e9
    set charge $match(charge)
    set match(sigma_z) 44.0
    set match(phase) 0.0
    set match(e_spread) -1.0

    set e0 $e_initial
    set e2 [expr $e0 * (1.0 + $deltae)]
    set e1 [expr $e0 * (1.0 - $deltae)]

    puts "e1 = $e1"
    puts "e2 = $e2"
    
    if { $log != 2 } {
	set n_slice 1
	set n 1
	set n_total [expr $n_slice*$n]
	
	make_beam_many beam0_$name $n_slice $n
	exec echo "$e0 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam0_$name

	make_beam_many beam2_$name $n_slice $n
	exec echo "$e2 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam2_$name

	make_beam_many beam1_$name $n_slice $n
	exec echo "$e1 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam1_$name
    } {
	set n_slice 100
	set n 1000
	set n_total [expr $n_slice*$n]

	make_beam_many beam0_$name $n_slice $n
	make_beam_many_energy beam2_$name $n_slice $n [expr 1. + $deltae ]
	make_beam_many_energy beam1_$name $n_slice $n [expr 1. - $deltae ]
    }
    
    set n_slice 100
    set n 1000
    set n_total [expr $n_slice*$n]
    
    make_beam_many beam0t_$name $n_slice $n
}

FirstOrder 1


#foreach name $beamline_list {
#if { $sr } {
#	SetReferenceEnergy -beamline $name -beam beam0t -survey Zero -iter 5
#} else {
#	SetReferenceEnergy -beamline $name -beam beam0 -survey Zero -iter 1
#}
#}

source $script_dir/scripts/octave_functions.tcl

source $script_dir/main/multipole_knobs_twobeam.tcl
#source align_multipoles_new.tcl
source $script_dir/main/align_multipoles_original.tcl

Octave {
    load '$script_dir/matrices/Response4_sr_${sr}_deltae_${deltae}.dat';
}

Octave {
    MI = placet_get_number_list("electron", "multipole")
    MS = placet_element_get_attribute("electron", MI, "strength");

    MI = MI(MS != 0);
    MS = placet_element_get_attribute("electron", MI, "strength");
    ML = placet_element_get_attribute("electron", MI, "length");
    MT = placet_element_get_attribute("electron", MI, "type");

    MRANGEX = zeros(size(MI));
    MRANGEY = zeros(size(MI));
    for multipole = 1:length(MI)
    _ms = abs(MS(multipole));
    _ml = ML(multipole);
    _mt = MT(multipole);
    deltakx = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Sxx(:,multipole)))) / $repeat;
    deltaky = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Syy(:,multipole)))) / $repeat;
    MRANGEX(multipole) = ((deltakx * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
    MRANGEY(multipole) = ((deltaky * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
    end
}

Octave {
    DI = placet_get_number_list("electron", "dipole");
    
    N = length(placet_get_number_list("electron", "bpm"));
    M = length(DI);
    
    Zero = zeros(N,M);
}

proc my_survey {beamline {beamlinenr 0} } {
    global machine sigma bpmres
    Octave {
	disp("beamline number")
	disp($beamlinenr)
	randn("seed", $machine * 1e4 + $beamlinenr); # + $beamline);
	BI = placet_get_number_list("$beamline", "bpm");
	DI = placet_get_number_list("$beamline", "dipole");
	QI = placet_get_number_list("$beamline", "quadrupole");
	placet_element_set_attribute("$beamline", DI, "strength_x", 0.0);
	placet_element_set_attribute("$beamline", DI, "strength_y", 0.0);
	placet_element_set_attribute("$beamline", BI, "resolution", $bpmres);
	placet_element_set_attribute("$beamline", BI, "x", randn(size(BI)) * $sigma);
	placet_element_set_attribute("$beamline", BI, "y", randn(size(BI)) * $sigma);
	placet_element_set_attribute("$beamline", QI, "x", randn(size(QI)) * $sigma);
	placet_element_set_attribute("$beamline", QI, "y", randn(size(QI)) * $sigma);
	placet_element_set_attribute("$beamline", MI, "x", randn(size(MI)) * $sigma);
	placet_element_set_attribute("$beamline", MI, "y", randn(size(MI)) * $sigma);
    }
}

Octave {
    % initialise storage arrays for the 7 tuning steps
    emittH   = zeros(2, 7);
    emittV   = zeros(2, 7);
    sizeH    = zeros(2, 7);
    sizeV    = zeros(2, 7);
    Lumi     = zeros(2, 7);
    LumiPeak = zeros(2, 7);
    LumiTwo     = zeros(1, 7);
    LumiTwoPeak = zeros(1, 7);
    IP = placet_get_name_number_list("electron", "IP");
}

foreach name $beamline_list {
    Octave {
	global Log$name;
	MaxSteps = 4 * ($iter1 + $iter2) + 4;
	Log$name.Chi     = zeros(MaxSteps, 1);
	Log$name.Chi_R   = zeros(MaxSteps, 1);
	Log$name.Chi_ETA = zeros(MaxSteps, 1);
	Log$name.Chi_CB1 = zeros(MaxSteps, 1);
	Log$name.Chi_CB2 = zeros(MaxSteps, 1);
	Log$name.EmittX  = zeros(MaxSteps, 1);
	Log$name.EmittY  = zeros(MaxSteps, 1);
	Log$name.SizeX   = zeros(MaxSteps, 1);
	Log$name.SizeY   = zeros(MaxSteps, 1);
	Log$name.Stage   = zeros(MaxSteps, 1);
	Log$name.index = 1;
    }
}

puts "MACHINE: $machine"

Octave {
    R0 = [ 
	  Response0.Rxx Zero
	  Zero Response0.Ryy
	 ];
    
    ETA0 = [
	    Response0.Eta_x Zero
	    Zero Response0.Eta_y
	   ];
    
    R1 = [ 
	  Response1.Rxx Zero
	  Zero Response1.Ryy
	 ];
    
    ETA1 = [
	    Response1.Eta_x Zero
	    Zero Response1.Eta_y
	   ];
    
    I0 = [
	  diag(ones(1,M)) zeros(M)
	  zeros(M) diag(ones(1,M))
	 ];
    
    i0 = zeros(2*M,1);
}

Octave {
    if $loadmachine
    for i=1:2
    machinefile = strcat(strcat("machine_status_",beamlinename(i,:)),"_$machine.dat")
    if exist(machinefile, "file")
    disp("reading machine status...");
    load machinefile;
    placet_element_set_attributes(beamlinename(i,:), STATUS);
    else
    disp("ERROR: machine status file not found");
    exit(1)
    end
    end
    end
}

puts "initial luminosity"

Octave {
    L= get_lumi()
}

foreach name $beamline_list {

    if { ! $loadmachine } {
	
	puts "Tracking, 1:1 and DFS"
	
	# misalign the machine
	puts "Random misalignment errors"
	if {[string compare $name electron] == 0} {
	    set beamlinenr 0
	} else {
	    set beamlinenr 1
	}
	
	my_survey $name $beamlinenr
	Octave {
	    
	    if $log == 2
	    [E,B] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	    else
	    [E,B] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	    end

	    #L= get_lumi(B)
	    
	    # multipoles off
	    disp("Multipoles off")
	    placet_element_set_attribute("$name", MI, "strength", complex(0.0, 0.0)); 

	    # 1:1 correction
	    #disp("Start of 1:1 correction...")
	    K0 = [ R0 ; $beta * I0 ];
	    #disp("Strengths matrix ok!")

	    for i=1:$iter1

	    placet_test_no_correction("$name", "beam0_$name", "None");
	    
	    #disp("tracking ok!")

	    Measure0.R = placet_get_bpm_readings("$name");

	    b0 = [
		  Measure0.R(:,1) - Response0.Model_R(:,1)
		  Measure0.R(:,2) - Response0.Model_R(:,2)
		 ];

	    k0 = -[ R0 ; $beta * I0 ] \ [ b0 ; i0 ];

	    placet_element_vary_attribute("$name", DI, "strength_x", $gain1 * k0(1:M));
	    placet_element_vary_attribute("$name", DI, "strength_y", $gain1 * k0((M+1):end));
	    chi1 = sum(b0 .** 2)
	    end
	    disp("End of 1:1 correction")
	    # end of 1:1
	    
	    # track a "fat" bunch beam0t to measure the beam size and the emittance
	    disp("Multipoles on")
	    placet_element_set_attribute("$name", MI, "strength", complex(MS,0.0));
	    if $log == 2
	    [E,B$name] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	    else
	    [E,B$name] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	    end
	    emittH($name,1) = E(end,2);
	    emittV($name,1) = E(end,6);
	    sizeH($name,1) = std((B$name)(:,2));
	    sizeV($name,1) = std((B$name)(:,3));
	    Lumi($name,1) = get_lumi((B$name))
	    LumiPeak($name,1) = str2num(Tcl_GetVar("lumi_peak"));

	    if (strcmp("$name","positron"))
	      LumiTwo(1,1) = get_lumi(Belectron,Bpositron)
	      LumiTwoPeak(1,1) = str2num(Tcl_GetVar("lumi_peak"));
	    end
	    
	    disp("Multipoles off")
	    placet_element_set_attribute("$name", MI, "strength", complex(0.0,0.0));
	    disp("Start of DFS")
	    # DFS (if weight != 0.)
	    if $w1 != 0

	    A0 = [ R0 ; $w1 * ETA0 ; $beta * I0 ];

	    for i=1:$iter2
	    placet_test_no_correction("$name", "beam0_$name", "None");

	    if $log == 2
	    Log$name.EmittX(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_x"));
	    Log$name.EmittY(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_y"));
	    Log$name.SizeX(Log$name.index) += str2num(Tcl_GetVar("beam_size_x"));
	    Log$name.SizeY(Log$name.index) += str2num(Tcl_GetVar("beam_size_y"));
	    end

	    Measure0.R = placet_get_bpm_readings("$name");
	    Measure0.Eta = MeasureDispersion("$name", "beam1_$name", "beam2_$name", $deltae);

	    b0 = [
		  Measure0.R(:,1) - Response0.Model_R(:,1)
		  Measure0.R(:,2) - Response0.Model_R(:,2)
		 ];

	    eta0 = [
		    Measure0.Eta(:,1) - Response0.Model_Eta(:,1)
		    Measure0.Eta(:,2) - Response0.Model_Eta(:,2)
		   ];

	    y0 =  [ b0 ; $w1 * eta0 ; $beta * i0 ];
	    k0 = -[ R0 ; $w1 * ETA0 ; $beta * I0 ] \ y0;

	    placet_element_vary_attribute("$name", DI, "strength_x", $gain2 * k0(1:M));
	    placet_element_vary_attribute("$name", DI, "strength_y", $gain2 * k0((M+1):end));

	    if $log
	    chi2 = sum(y0 .** 2)
	    chi_R = sum(b0 .** 2);
	    chi_ETA = sum(eta0 .** 2);

	    Log$name.Chi(Log$name.index) += chi2;
	    Log$name.Chi_R(Log$name.index) += chi_R;
	    Log$name.Chi_ETA(Log$name.index) += chi_ETA;
	    Log$name.Stage(Log$name.index) = 2;
	    Log$name.index++;
	    end
	    end
	    end
	    disp("End of DFS")
	}

	# end of DFS

        # measure the beam size and the emittance

	Octave {
	    placet_element_set_attribute("$name", MI, "strength", complex(MS,0.0));
	    if $log == 2
	    [E,B$name] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	    else
	    [E,B$name] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	    end
	    emittH($name,2) = E(end,2);
	    emittV($name,2) = E(end,6);
	    sizeH($name,2) = std((B$name)(:,2));
	    sizeV($name,2) = std((B$name)(:,3));
	    Lumi($name,2) = get_lumi((B$name))
	    LumiPeak($name,2) = str2num(Tcl_GetVar("lumi_peak"));

	    if (strcmp("$name","positron"))
	      LumiTwo(1,2) = get_lumi(Belectron,Bpositron)
	      LumiTwoPeak(1,2) = str2num(Tcl_GetVar("lumi_peak"));
	    end

	    placet_element_set_attribute("$name", MI, "strength", complex(0.0,0.0));
	}

	
	#puts "align multipoles 1"

	#align_multipoles
	
  	# switch on multipoles
	Octave {
	    placet_element_set_attribute("$name", MI, "strength", complex(MS,0.0));
	}
	
  	# measure the beam size and the emittance
	Octave {
	    if $log == 2
	    [E,B$name] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	    else
	    [E,B$name] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	    end
	    emittH($name,3) = E(end,2);
	    emittV($name,3) = E(end,6);
	    sizeH($name,3) = std((B$name)(:,2));
	    sizeV($name,3) = std((B$name)(:,3));
	    Lumi($name,3) = get_lumi((B$name))
	    LumiPeak($name,3) = str2num(Tcl_GetVar("lumi_peak"));

	    if (strcmp("$name","positron"))
	      LumiTwo(1,3) = get_lumi(Belectron,Bpositron)
	      LumiTwoPeak(1,3) = str2num(Tcl_GetVar("lumi_peak"));
	    end
	}
        
	Octave {
	    STATUS = placet_element_get_attributes("$name");
	    save -text machine_status_${name}_${machine}.dat STATUS Lumi
	}
    }	
}

puts "Initialise knobs"

if { ! $loadmachine } {

    if { $log == 2 } {
	multipole_knobs 10.0
    } else {
	puts "no fast knobs for two beam"
    }
    # end of first stage alignment (skipped when reading in machine)
}

puts "Final knobs"

puts "tracking"

foreach name $beamline_list {
    Octave {
	if $log == 2
	[E,B$name] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	else
	[E,B$name] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	end
	emittH($name,4) = E(end,2);
	emittV($name,4) = E(end,6);
	sizeH($name,4) = std((B$name)(:,2));
	sizeV($name,4) = std((B$name)(:,3));
	Lumi($name,4) = get_lumi((B$name))
	LumiPeak($name,4) = str2num(Tcl_GetVar("lumi_peak"));
    
	if (strcmp("$name","positron"))
	  LumiTwo(1,4) = get_lumi(Belectron,Bpositron)
	  LumiTwoPeak(1,4) = str2num(Tcl_GetVar("lumi_peak"));
	end

	% save to be sure 
	save -text Lumi.dat Lumi;
	save -text LumiTwo.dat LumiTwo;
    }
}

puts "final tracking"

foreach name $beamline_list {
    Octave {
	if $log == 2
	placet_test_no_correction("$name", "beam0_$name", "None");
	Log$name.EmittX(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_x"));
	Log$name.EmittY(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_y"));
	Log$name.SizeX(Log$name.index) += str2num(Tcl_GetVar("beam_size_x"));
	Log$name.SizeY(Log$name.index) += str2num(Tcl_GetVar("beam_size_y"));
	Log$name.Stage(Log$name.index) = 4;
	Log$name.index++;
	end
    }
}
    
#Octave {
#STATUS = placet_element_get_attributes("test");
#save -text machine_status_$machine.dat STATUS Lumi
#}


foreach name $beamline_list {
puts "Start second DFS $name"
    Octave {
	if $w2 != 0
	
	A1 = [ R1 ; $w2 * ETA1 ; $beta * I0 ];
	
	for i=1:$iter2
	
	#printf("tracking beam0\n");
	placet_test_no_correction("$name", "beam0_$name", "None");
	
	if $log == 2
	Log$name.EmittX(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_x"));
	Log$name.EmittY(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_y"));
	Log$name.SizeX(Log$name.index) += str2num(Tcl_GetVar("beam_size_x"));
	Log$name.SizeY(Log$name.index) += str2num(Tcl_GetVar("beam_size_y"));
	end
	
	#printf("measuring dispersion\n");
	Measure1.R = placet_get_bpm_readings("$name");
	Measure1.Eta = MeasureDispersion("$name", "beam1_$name", "beam2_$name", $deltae);
	
	b1 = [
	      Measure1.R(:,1) - Response1.Model_R(:,1)
	      Measure1.R(:,2) - Response1.Model_R(:,2)
	     ];

	eta1 = [
		Measure1.Eta(:,1) - Response1.Model_Eta(:,1)
		Measure1.Eta(:,2) - Response1.Model_Eta(:,2)
	       ];

	y1 =  [ b1 ; $w2 * eta1 ; $beta * i0 ];
	k1 = -[ R1 ; $w2 * ETA1 ; $beta * I0 ] \ y1;

	placet_element_vary_attribute("$name", DI, "strength_x", $gain2 * k1(1:M));
	placet_element_vary_attribute("$name", DI, "strength_y", $gain2 * k1((M+1):end));
        
	if $log				
	chi3 = sum(y1 .** 2)
	chi_R = sum(b1 .** 2);
	chi_ETA = sum(eta1 .** 2);

	Log$name.Chi(Log$name.index) += chi3;
	Log$name.Chi_R(Log$name.index) += chi_R;
	Log$name.Chi_ETA(Log$name.index) += chi_ETA;
	Log$name.Stage(Log$name.index) = 5;
	Log$name.index++;
	end
	end
	end
    }

    puts "Second DFS end $name"
}

foreach name $beamline_list {
    Octave {
	if $log == 2
	[E,B$name] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	else
	[E,B$name] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	end
	emittH($name,5) = E(end,2);
	emittV($name,5) = E(end,6);
	sizeH($name,5) = std((B$name)(:,2));
	sizeV($name,5) = std((B$name)(:,3));
	Lumi($name,5) = get_lumi((B$name))
	LumiPeak($name,5) = str2num(Tcl_GetVar("lumi_peak"));

	if (strcmp("$name","positron"))
	  LumiTwo(1,5) = get_lumi(Belectron,Bpositron)
	  LumiTwoPeak(1,5) = str2num(Tcl_GetVar("lumi_peak"));
	end
    }
    
    #align_multipoles
    
    Octave {
	if $log == 2
	[E,B$name] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	else
	[E,B$name] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	end
	emittH($name,6) = E(end,2);
	emittV($name,6) = E(end,6);
	sizeH($name,6) = std((B$name)(:,2));
	sizeV($name,6) = std((B$name)(:,3));
	Lumi($name,6) = get_lumi((B$name))
	LumiPeak($name,6) = str2num(Tcl_GetVar("lumi_peak"));
	if (strcmp("$name","positron"))
	  LumiTwo(1,6) = get_lumi(Belectron,Bpositron)
	  LumiTwoPeak(1,6) = str2num(Tcl_GetVar("lumi_peak"));
	end
    }
}

if { $log == 2 } {
    multipole_knobs 5.0
}

foreach name $beamline_list {
    Octave {
	if $log == 2
	[E,B$name] = placet_test_no_correction("$name", "beam0t_$name", "None",1,0,IP);
	else
	[E,B$name] = placet_test_no_correction("$name", "beam0_$name", "None",1,0,IP);
	end
	emittH($name,7) = E(end,2);
	emittV($name,7) = E(end,6);
	sizeH($name,7) = std((B$name)(:,2));
	sizeV($name,7) = std((B$name)(:,3));
	Lumi($name,7) = get_lumi((B$name))
	LumiPeak($name,7) = str2num(Tcl_GetVar("lumi_peak"));
	if (strcmp("$name","positron"))
	  LumiTwo(1,7) = get_lumi(Belectron,Bpositron)
	  LumiTwoPeak(1,7) = str2num(Tcl_GetVar("lumi_peak"));
	end
    }
}

foreach name $beamline_list {
    Octave {
	if $log == 2
	placet_test_no_correction("$name", "beam0_$name", "None");
	Log$name.EmittX(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_x"));
	Log$name.EmittY(Log$name.index) += str2num(Tcl_GetVar("beam_emitt_y"));
	Log$name.SizeX(Log$name.index) += str2num(Tcl_GetVar("beam_size_x"));
	Log$name.SizeY(Log$name.index) += str2num(Tcl_GetVar("beam_size_y"));
	Log$name.Stage(Log$name.index) = 7;
	Log$name.index++;
	end
    }

    Octave {
  	STATUS = placet_element_get_attributes("$name");
  	save -text machine_status_$name.dat STATUS Lumi
    }

    Octave {
	printf("RESULT: sizex %g %g %g %g %g %g %g\n", sizeH($name,1), sizeH($name,2), sizeH($name,3), sizeH($name,4), sizeH($name,5), sizeH($name,6), sizeH($name,7));
	printf("RESULT: sizey %g %g %g %g %g %g %g\n", sizeV($name,1), sizeV($name,2), sizeV($name,3), sizeV($name,4), sizeV($name,5), sizeV($name,6), sizeV($name,7));
	printf("RESULT: luminosity %g %g %g %g %g %g %g\n", Lumi($name,1), Lumi($name,2), Lumi($name,3), Lumi($name,4), Lumi($name,5), Lumi($name,6), Lumi($name,7));

        save -text emittH.dat emittH;
	save -text emittV.dat emittV;
	save -text sizeH.dat sizeH;
	save -text sizeV.dat sizeV;
	save -text Lumi.dat Lumi;
	save -text LumiPeak.dat LumiPeak;
	save -text LumiTwo.dat LumiTwo;
	save -text LumiTwoPeak.dat LumiTwoPeak;
	file = fopen("beamsize.out", "w");
	fprintf(file, " %.15g %.15g\n", sizeH($name,7), sizeV($name,7));
	fclose(file);
    }
}

foreach name $beamline_list {
    if { $log } {
	Octave {
	    T = [ Log$name.Chi Log$name.Chi_R Log$name.Chi_ETA Log$name.Chi_CB1 Log$name.Chi_CB2 Log$name.EmittX Log$name.EmittY Log$name.SizeX Log$name.SizeY Log$name.Stage];
	    save -text correct$name.log T
	}
    }
}

#Octave {
#	system("rm machine_status_$machine.dat")
#}
