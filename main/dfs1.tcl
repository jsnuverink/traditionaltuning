puts "DFS, multipoles off"

Octave {
    disp("Multipoles off")
    placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));
    disp("Start of DFS")
    # DFS (if weight != 0.)
    if $w1 != 0

    #A0 = [ R0 ; $w1 * ETA0 ; $beta * I0 ];
    
    for i=1:$iter2
    placet_test_no_correction("test", "beam0", "None");
    
    Log.EmittX(Log.index) += str2num(Tcl_GetVar("beam_emitt_x"));
    Log.EmittY(Log.index) += str2num(Tcl_GetVar("beam_emitt_y"));
    Log.SizeX(Log.index) += str2num(Tcl_GetVar("beam_size_x"));
    Log.SizeY(Log.index) += str2num(Tcl_GetVar("beam_size_y"));
    
    Measure0.R = placet_get_bpm_readings("test");
    Measure0.Eta = MeasureDispersion("test", "beam1", "beam2", $deltae);
    
    b0 = [
	  Measure0.R(:,1) - Response0.Model_R(:,1)
	  Measure0.R(:,2) - Response0.Model_R(:,2)
	 ];
    
    eta0 = [
	    Measure0.Eta(:,1) - Response0.Model_Eta(:,1)
	    Measure0.Eta(:,2) - Response0.Model_Eta(:,2)
	   ];
    
    y0 =  [ b0 ; $w1 * eta0 ; $beta * i0 ];
    k0 = -[ R0 ; $w1 * ETA0 ; $beta * I0 ] \ y0;
    
    placet_element_vary_attribute("test", DI, "strength_x", $gain2 * k0(1:M));
    placet_element_vary_attribute("test", DI, "strength_y", $gain2 * k0((M+1):end));
    
    chi2 = sum(y0 .** 2)
    chi_R = sum(b0 .** 2);
    chi_ETA = sum(eta0 .** 2);

    Log.Chi(Log.index) += chi2;
    Log.Chi_R(Log.index) += chi_R;
    Log.Chi_ETA(Log.index) += chi_ETA;
    Log.Stage(Log.index) = 2;
    Log.index++;
    end
    end
    disp("End of DFS")

    disp("Multipoles on")
    placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));

    # measure the beam size and the emittance
    
    saveLumi(2);
    STATUS = placet_element_get_attributes("test");
    save -text machine_status_dfs1.dat STATUS;
}
