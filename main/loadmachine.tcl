Octave {
    if exist("machine_status_$machine.dat", "file")
    disp("reading machine status...");
    load 'machine_status_$machine.dat';
    placet_element_set_attributes("test", STATUS);
    else
    disp("ERROR: machine status file not found");
    exit(1);
    end
}
