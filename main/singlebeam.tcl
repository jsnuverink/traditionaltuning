#############################################################################
#
# Tuning studies for single beam
#
#############################################################################

# Disable multi-core option 
#ParallelThreads -num 1 

set e_initial 1500
set e0 $e_initial

set script_dir /afs/cern.ch/user/r/rbodenst/tuning/traditionaltuning_modular/traditionaltuning/
if {![file exist $script_dir]} {
    set script_dir [pwd]/..
}

# command-line options

# sr          : synchrotron radiation on/off
# gain1       : gain 1-to-1 steering
# gain2       : gain dfs
# iter1       : nr of iterations 1-to-1 steering
# iter2       : nr of iterations dfs
# sigma       : misalignment in um
# bpmres      : bpm resolution in um
# deltae      : energy difference for dfs
# beta        : normalisation parameter for dfs
# w1          : weight for first stage dfs
# w2          : weight for second stage dfs
# machine     : machine seed
# loadmachine : load machine status from file on/off
# repeat      : multipole alignment parameter
# dfs1        : first stage dfs on/off
# dfs2/dfs2knobs/dfs2hybrid  : second stage dfs: either dfs / dfs knobs / hybrid method
# dfsmeasure  : measure response matrix 
# secondorder : apply second order knobs

array set args {
    sr 1
    gain1 0.5
    gain2 0.2
    iter1 100
    iter2 36
    sigma 10.0
    bpmres 0.010
    deltae 0.001
    beta 11
    w1 73
    w2 5
    machine 1
    loadmachine 0
    repeat 1
    dfs1 1
    dfs2 0
    dfs2knobs 0
    dfs2hybrid 1
    dfsmeasure 1
    secondorder 0
}

array set args $argv

set gain1 $args(gain1)
set gain2 $args(gain2)
set iter1 $args(iter1)
set iter2 $args(iter2)
set sigma $args(sigma)
set bpmres $args(bpmres)
set deltae $args(deltae)
set w1 $args(w1)
set w2 $args(w2)
set beta $args(beta)
set sr $args(sr)
set machine $args(machine)
set loadmachine $args(loadmachine)
set repeat $args(repeat)
set dfs1 $args(dfs1)
set dfs2 $args(dfs2)
set dfs2knobs $args(dfs2knobs)
set dfs2hybrid $args(dfs2hybrid)
set dfsmeasure $args(dfsmeasure)
set secondorder $args(secondorder)

if { $w2 == -1 } {
    set w2 $w1
}

# load lattice
source $script_dir/main/loadlattice.tcl

# create beams
source $script_dir/main/createbeams.tcl

FirstOrder 1

puts "Measuring dispersion"

Octave {
    function Eta = MeasureDispersion(Beamline, Beam1, Beam2, delta)
    placet_test_no_correction(Beamline, Beam2, "None"); Eta  = placet_get_bpm_readings(Beamline);
    placet_test_no_correction(Beamline, Beam1, "None"); Eta -= placet_get_bpm_readings(Beamline);
    Eta /= 2 * delta;
    end
}

puts "Measuring Beta Beating"

Octave {
    function CB = MeasureCouplingBetabeating(Beamline, Beam, Corrector, Leverage, Kick)
    K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
    placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
    placet_test_no_correction(Beamline, Beam, "None");
    CB=placet_get_bpm_readings(Beamline);
    placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
    placet_test_no_correction(Beamline, Beam, "None");
    CB-=placet_get_bpm_readings(Beamline);
    placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
    CB/=2*Kick;
    end
}

# load knobs
source $script_dir/main/multipole_knobs.tcl
#source $script_dir/main/align_multipoles_original.tcl

# load DFS
source $script_dir/main/dfs_loadmatrices.tcl

# inspect lattice
source $script_dir/main/element_indices.tcl

Octave {
    % initialise storage arrays for the tuning steps
    tuningSteps = 8
    global emittH   = zeros(1, tuningSteps);
    global emittV   = zeros(1, tuningSteps);
    global sizeH    = zeros(1, tuningSteps);
    global sizeV    = zeros(1, tuningSteps);
    global Lumi     = zeros(1, tuningSteps);
    global LumiPeak = zeros(1, tuningSteps);
    global IP = placet_get_name_number_list("test", "IP");
}

Octave {
    global Log;
    MaxSteps = 4 * ($iter1 + $iter2) + 4;
    Log.Chi     = zeros(MaxSteps, 1);
    Log.Chi_R   = zeros(MaxSteps, 1);
    Log.Chi_ETA = zeros(MaxSteps, 1);
    Log.Chi_CB1 = zeros(MaxSteps, 1);
    Log.Chi_CB2 = zeros(MaxSteps, 1);
    Log.EmittX  = zeros(MaxSteps, 1);
    Log.EmittY  = zeros(MaxSteps, 1);
    Log.SizeX   = zeros(MaxSteps, 1);
    Log.SizeY   = zeros(MaxSteps, 1);
    Log.Stage   = zeros(MaxSteps, 1);

    deltae = $deltae
}

puts "MACHINE: $machine"

Octave {
    Log.index = 1;
}

if {$loadmachine} {
    source $script_dir/main/loadmachine.tcl
}

puts "initial luminosity"

Octave {
    [E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
    L= get_lumi(B)
}

if { ! $loadmachine} {

    # misalign the machine
    puts "Random misalignment errors"
    proc my_survey {} {
	global machine sigma bpmres
	Octave {
	    randn("seed", $machine * 1e4);
	    BI = placet_get_number_list("test", "bpm");
	    DI = placet_get_number_list("test", "dipole");
	    QI = placet_get_number_list("test", "quadrupole");
	    placet_element_set_attribute("test", DI, "strength_x", 0.0);
	    placet_element_set_attribute("test", DI, "strength_y", 0.0);
	    placet_element_set_attribute("test", BI, "resolution", $bpmres);
	    placet_element_set_attribute("test", BI, "x", randn(size(BI)) * $sigma);
	    placet_element_set_attribute("test", BI, "y", randn(size(BI)) * $sigma);
	    placet_element_set_attribute("test", QI, "x", randn(size(QI)) * $sigma);
	    placet_element_set_attribute("test", QI, "y", randn(size(QI)) * $sigma);
	    placet_element_set_attribute("test", MI, "x", randn(size(MI)) * $sigma);
	    placet_element_set_attribute("test", MI, "y", randn(size(MI)) * $sigma);
	}
    }

    my_survey

    # measure response
    if { $dfsmeasure } {
	source $script_dir/main/measure_response.tcl
    }

    # 1 to 1 steering
    source $script_dir/main/121.tcl
    
    if { $dfs1 } {
	source $script_dir/main/dfs1.tcl
    }

    #Octave {
    #	placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));
    #}
    #puts "align multipoles 1"

    #align_multipoles
    
    # switch on multipoles
    #Octave {
    #placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));
    #}
    
    # measure the beam size and the emittance
    #Octave {
    #saveLumi(3);
    #}
    
    #Octave {
    #	STATUS = placet_element_get_attributes("test");
    #	save -text machine_status_$machine.dat STATUS
    #   }
    
    puts "Initialise knobs"
    
    multipole_knobs 10.0

    Octave {
	STATUS = placet_element_get_attributes("test");
	save -text machine_status_knobs1.dat STATUS;
    }
}

# end of first stage alignment (skipped when reading in machine)
Octave {
    saveLumi(4);
}

# DFS2 algorithm: pure DFS, DFS knobs, or hybrid (pure DFS if lumi smaller than 3%)
if {$dfs2} {
    source $script_dir/main/dfs2.tcl
} elseif {$dfs2knobs} {
    dfs_knobs 5
} elseif {$dfs2hybrid} {
    Octave {
	Lumirel = LumiPeak(1,4) * 312 * 50 / 5.9e34 / 1e4
	Tcl_SetVar("lumirel",Lumirel)
    }
    if { $lumirel >= 0.03} {
	dfs_knobs 5
    } else {
	source $script_dir/main/dfs2.tcl
    }
}

Octave {
    saveLumi(5);
}

#align_multipoles
#Octave {
#    saveLumi(6);
#}

multipole_knobs 5.0

Octave {
    saveLumi(7);
}

if {$secondorder} {
    ## sextupole tilts, not improving
    # tilt_knobs 5
    ## sextupole strengths
    strength_knobs 50
}

Octave {
    saveLumi(8);
}

# Save results
Octave {
    STATUS = placet_element_get_attributes("test");
    save -text machine_status.dat STATUS

    save -text emittH.dat emittH;
    save -text emittV.dat emittV;
    save -text sizeH.dat sizeH;
    save -text sizeV.dat sizeV;
    save -text Lumi.dat Lumi;
    save -text LumiPeak.dat LumiPeak;

    T = [ Log.Chi Log.Chi_R Log.Chi_ETA Log.Chi_CB1 Log.Chi_CB2 Log.EmittX Log.EmittY Log.SizeX Log.SizeY Log.Stage];
    save -text correct.log T
}
