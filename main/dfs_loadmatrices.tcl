Octave {
    load '$script_dir/matrices/Response4_sr_${sr}_deltae_${deltae}.dat';

    global DI;
    DI = placet_get_number_list("test", "dipole");
    
    N = length(placet_get_number_list("test", "bpm"));
    M = length(DI);
    
    Zero = zeros(N,M);
}

source $script_dir/main/dfs_constructmatrices.tcl
