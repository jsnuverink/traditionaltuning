loadmachine=1

for sr in 1 ; do
for bpmres in 0.010 ; do
for machine in `seq 1 110` ; do

startdir=/Users/jsnuverink/cernbox/BeamTuning/corr8_sr_1_beta_11_w1_73_w2_5_lumi_total-first_pass_110m/seed_${bpmres}_${machine}

echo "$startdir"

#out_dir = ${startdir}

if test -e ${startdir}/machine_status.dat.gz ; then
  gzcat ${startdir}/machine_status.dat.gz > machine_status_${machine}.dat
fi

placet singleshot.tcl bpmres $bpmres machine $machine log 2 sr $sr loadmachine $loadmachine

cp electron.ini $startdir
cp positron.ini $startdir

done
done
done
