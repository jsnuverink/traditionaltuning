nm=1
beta=17
w1=197
w2=463


for sr in 1 ; do

out_dir=/afs/cern.ch/user/h/hgarciam/public/CLIC_3TeV_lattices/trad_reopt/tuning_betas_500_sext_0.7/results/corr8_sr_${sr}_beta_${beta}_w1_${w1}_w2_${w2}_lumi_total-20140318

mkdir -p ${out_dir}

for queue in 2nw ; do
for bpmres in 0.010 ; do
for machine in `seq 1 110` ; do
#for machine in 17 30 53 ; do

#placet singlebeam.tcl bpmres $bpmres nm $nm machine $machine log 2 sr $sr beta $beta w1 $w1 w2 $w2 iter2 40 gain2 0.2 2>&1 | tee output

bsub -q $queue << EOF
source /afs/cern.ch/sw/lcg/contrib/gcc/4.8/x86_64-slc6-gcc48-opt/setup.sh
cp $PWD/* .

placet singlebeam.tcl bpmres $bpmres nm $nm machine $machine log 2 sr $sr beta $beta w1 $w1 w2 $w2 iter2 100 gain2 0.2 2>&1 | tee output

tail -n 40 output > ${out_dir}/machine_bpmres_${bpmres}_${machine}.dat

cp correct.log ${out_dir}/correct_bpmres_${bpmres}_${machine}.log
cp machine_status.dat ${out_dir}/machine_status_${machine}.dat

EOF

done
done
done
done
