beta=11
w1=73
w2=5

for sr in 1 ; do
for queue in 2nw ; do
for bpmres in 0.010 ; do
for machine in `seq 1 110` ; do
#for machine in 18 48 74 78 80 82 90 95 97 101 110; do

out_dir=/afs/cern.ch/user/j/jsnuveri/work/traditional/results/corr8_sr_${sr}_beta_${beta}_w1_${w1}_w2_${w2}_lumi_total-first_pass_twobeam/seed_${bpmres}_${machine}

mkdir -p ${out_dir}

bsub -q $queue << EOF

cp $PWD/* .

if test -e ${out_dir}/machine_status_bpmres_${bpmres}_${machine}.dat.gz ; then
  zcat ${out_dir}/machine_status_bpmres_${bpmres}_${machine}.dat.gz > machine_status_${machine}.dat
fi

source /afs/cern.ch/user/j/jsnuveri/setup.sh

placet twobeam.tcl bpmres $bpmres machine $machine log 2 sr $sr beta $beta w1 $w1 w2 $w2 2>&1 | tee output

# what is this for? - JS
tail -n 40 output > ${out_dir}/machine.dat

cp output ${out_dir}/output.log

gzip -9 machine_status.dat

cp machine_status.dat.gz ${out_dir}/machine_status.dat.gz

cp correct.log ${out_dir}/correct.log

cp emitt*.dat ${out_dir}/.

cp size*.dat ${out_dir}/.

cp Lumi.dat ${out_dir}/.

cp knobs.dat ${out_dir}/.

EOF

done
done
done
done
