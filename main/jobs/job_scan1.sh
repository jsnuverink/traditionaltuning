sr=0

for nm in 40 ; do
for beta in `seq 0 20` ; do
#for beta in 10; do
for bpmres in 0.010 ; do

#out_dir=~/w0/CLIC_BDS_Alignment-Knobs/results/corr8_scan_sr_${sr}_nm_${nm}_bpmres_${bpmres}
out_dir=~/public/tuning/results/corr8_scan_sr_${sr}_nm_${nm}_bpmres_${bpmres}

mkdir -p $out_dir

bsub -q 2nw << EOF

cp $PWD/* .

cat > pippo.tcl << FILE
Octave {
  
  format long
	
  function S = Beamsize(x)

    w1 = exp(x(1));
    w2 = exp(x(2));

    command = sprintf("placet-octave singlebeam.tcl bpmres $bpmres w1 %g w2 %g nm $nm log 0 beta $beta ", w1, w2);
    
    system(command);

    S = load 'beamsize.out';
    
    printf("DFS %g %g : %g %g\n", w1, w2, S(1), S(2));

    S = (S(1)/45)**2 + (S(2)/1)**2;

  end

  x = fmins("Beamsize", [ 0. 0. ]);
  w1 = exp(x(1))
  w2 = exp(x(2))
  S = load 'beamsize.out';
  printf("SIZEX : %.15g +/- %.15g\n", S(1), S(3));
  printf("SIZEY : %.15g +/- %.15g\n", S(2), S(4));
}
FILE

placet-octave pippo.tcl | tail -n 20 | tee ${out_dir}/scan_nm_${nm}_bpmres_${bpmres}_beta_${beta}.dat
EOF

done
done
done
