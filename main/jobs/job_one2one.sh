sr=1
nm=40
bpmres=0.010

out_dir=~/private/tuning/results/one2one_scan

mkdir -p ${out_dir}

#for beta in `seq 0 20` ; do
for betax in `seq 1 5` ; do
for betay in `seq 1 20` ; do

bsub -q 2nw -e /dev/null -o /dev/null << EOF

cp $PWD/* .
rm beamsize.dat
placet-octave correct10.tcl bpmres $bpmres nm $nm log 2 betax $betax betay $betay sr $sr
cat beamsize.out > scan_one_to_one_bx_by.dat
cp scan_one_to_one_bx_by.dat $out_dir/scan_one_to_one_bx_by_${betax}_${betay}.dat
EOF
done
done
