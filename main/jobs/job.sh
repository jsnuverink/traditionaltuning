secondorder=1

for queue in 2nw ; do
for bpmres in 0.010 ; do
#for machine in `seq 1 110` ; do
for machine in 1; do

out_dir=/afs/cern.ch/user/j/jsnuveri/work/traditional/results/singlebeam_1st/seed_${bpmres}_${machine}

mkdir -p ${out_dir}

bsub -q $queue << EOF

cp $PWD/* .

if test -e ${out_dir}/machine_status_bpmres_${bpmres}_${machine}.dat.gz ; then
  zcat ${out_dir}/machine_status_bpmres_${bpmres}_${machine}.dat.gz > machine_status_${machine}.dat
fi

source /afs/cern.ch/user/j/jsnuveri/setup.sh

placet singlebeam.tcl bpmres $bpmres machine $machine secondorder $secondorder 2>&1 | tee output

# what is this for? - JS
tail -n 40 output > ${out_dir}/machine.dat

cp output ${out_dir}/output.log

gzip -9 machine_status.dat

cp machine_status.dat.gz ${out_dir}/machine_status.dat.gz

cp machine_status*.dat ${out_dir}/.

cp correct.log ${out_dir}/correct.log

cp emitt*.dat ${out_dir}/.

cp size*.dat ${out_dir}/.

cp Lumi.dat ${out_dir}/.

cp knobs.dat ${out_dir}/.

cp *tron.ini ${out_dir}/.

EOF

done
done
done
