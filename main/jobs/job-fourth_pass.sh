nm=110
beta=10
w1=635
w2=11

CURRENT=fourth
PREVIOUS=third

for sr in 1 ; do

pre_dir=~/w0/CLIC_BDS_Alignment-Knobs/results-escaling-lpeak/corr8_sr_${sr}_beta_${beta}_w1_${w1}_w2_${w2}_lumi_total-${PREVIOUS}_pass
out_dir=~/w0/CLIC_BDS_Alignment-Knobs/results-escaling-lpeak/corr8_sr_${sr}_beta_${beta}_w1_${w1}_w2_${w2}_lumi_total-${CURRENT}_pass

mkdir -p ${out_dir}

for queue in 2nw ; do
for bpmres in 0.010 ; do
for machine in `seq 1 $nm` ; do

bsub -q $queue -e /dev/null -o /dev/null << EOF

cp $PWD/* .

if test -e ${pre_dir}/machine_status_bpmres_${bpmres}_${machine}.dat.gz ; then
  zcat ${pre_dir}/machine_status_bpmres_${bpmres}_${machine}.dat.gz > machine_status.dat

  placet-octave singlebeam.tcl bpmres $bpmres nm $nm machine $machine log 2 sr $sr beta $beta w1 $w1 w2 $w2 iter2 40 gain2 0.2 2>&1 | tee output

  tail -n 40 output > ${out_dir}/machine_bpmres_${bpmres}_${machine}.dat

  gzip -9 machine_status.dat

  cp machine_status.dat.gz ${out_dir}/machine_status_bpmres_${bpmres}_${machine}.dat.gz

  cp correct.log ${out_dir}/correct_bpmres_${bpmres}_${machine}.log
fi

EOF

done
done
done
done
