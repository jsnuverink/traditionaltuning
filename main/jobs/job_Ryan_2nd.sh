loadmachine=1
beta=11
w1=73
w2=5
dfs1=1
dfs2hybrid=1
secondorder=0

for sr in 1 ; do
for queue in 1nw ; do
for bpmres in 0.010 ; do
#for machine in 47; do
#for machine in `seq 1 110` ; do
for machine in 66 71 75 76 77 79 80 87 89 91 93 95 98 100; do

start_dir=/afs/cern.ch/work/r/rbodenst/SingleBeamtuningresults/DFS_DFSHybrid_1/seed_${bpmres}_${machine}

out_dir=/afs/cern.ch/work/r/rbodenst/SingleBeamtuningresults/DFS_DFSHybrid_2/seed_${bpmres}_${machine}

mkdir -p ${out_dir}

bsub -q $queue << EOF

cp $PWD/* .

if test -e ${start_dir}/machine_status.dat.gz ; then
  zcat ${start_dir}/machine_status.dat.gz > machine_status_${machine}.dat
fi

source /afs/cern.ch/user/r/rbodenst/setup.sh

placet singlebeam.tcl bpmres $bpmres machine $machine sr $sr beta $beta w1 $w1 w2 $w2 dfs1 $dfs1 dfs2hybrid $dfs2hybrid secondorder $secondorder loadmachine $loadmachine 2>&1 | tee output

# what is this for? - JS
tail -n 40 output > ${out_dir}/machine.dat

cp output ${out_dir}/output.log

gzip -9 machine_status.dat

cp machine_status.dat.gz ${out_dir}/machine_status.dat.gz

cp machine_status*.dat ${out_dir}/.

cp correct.log ${out_dir}/correct.log

cp emitt*.dat ${out_dir}/.

cp size*.dat ${out_dir}/.

cp Lumi.dat ${out_dir}/.

cp knobs.dat ${out_dir}/.

cp electron.ini ${out_dir}/.

cp positron.ini ${out_dir}/.

EOF

done
done
done
done
