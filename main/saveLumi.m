%% function that tracks and gets lumi and saves data in relevant arrays
%%

function [E,B] = saveLumi(index)

global emittH emittV sizeH sizeV Lumi LumiPeak IP;

[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
emittH(1,index) = E(end,2);
emittV(1,index) = E(end,6);
sizeH(1,index) = std(B(:,2));
sizeV(1,index) = std(B(:,3));
Lumi(1,index) = get_lumi(B)
LumiPeak(1,index) = str2num(Tcl_GetVar("lumi_peak"));

end