puts "1:1 correction"

Octave {
    # multipoles off
    disp("Multipoles off")
    placet_element_set_attribute("test", MI, "strength", complex(0.0, 0.0)); 

    # 1:1 correction
    #disp("Start of 1:1 correction...")
    K0 = [ R0 ; $beta * I0 ];
    #disp("Strengths matrix ok!")
    
    for i=1:$iter1
    
    placet_test_no_correction("test", "beam0", "None");

    Log.EmittX(Log.index) += str2num(Tcl_GetVar("beam_emitt_x"));
    Log.EmittY(Log.index) += str2num(Tcl_GetVar("beam_emitt_y"));
    Log.SizeX(Log.index) += str2num(Tcl_GetVar("beam_size_x"));
    Log.SizeY(Log.index) += str2num(Tcl_GetVar("beam_size_y"));

    Measure0.R = placet_get_bpm_readings("test");
    
    b0 = [
	  Measure0.R(:,1) - Response0.Model_R(:,1)
	  Measure0.R(:,2) - Response0.Model_R(:,2)
	 ];
    
    k0 = -[ R0 ; $beta * I0 ] \ [ b0 ; i0 ];
    
    placet_element_vary_attribute("test", DI, "strength_x", $gain1 * k0(1:M));
    placet_element_vary_attribute("test", DI, "strength_y", $gain1 * k0((M+1):end));

    chi2 = sum(b0 .** 2)

    Log.Chi(Log.index) += chi2;
    Log.Stage(Log.index) = 1;
    Log.index++;
    
    end
    disp("End of 1:1 correction")
    # end of 1:1
    
    # track a "fat" bunch beam0t to measure the beam size and the emittance
    disp("Multipoles on")
    placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));
    saveLumi(1);

    STATUS = placet_element_get_attributes("test");
    save -text machine_status_121.dat STATUS;
}
