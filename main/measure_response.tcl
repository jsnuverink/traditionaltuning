# Measure response matrix
puts "Measure Response matrices"

Octave {
    disp("Multipoles off")
    placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));

    [Rxx, Rxy, Ryx, Ryy, Rxxx,Rxxy, Rxyy, Ryxx, Ryyy] = placet_get_response_matrix_fit("test","beam0","None",BI,DI);

    # overwrite response from file
    Response0.Rxx = Rxx;
    Response0.Ryy = Ryy;

    [Rxx1, Rxy1, Ryx1, Ryy1, Rxxx1, Rxxy1, Rxyy1, Ryxx1, Ryyy1] = placet_get_response_matrix_fit("test","beam1","None",BI,DI);

    [Rxx2, Rxy2, Ryx2, Ryy2, Rxxx2, Rxxy2, Rxyy2, Ryxx2, Ryyy2] = placet_get_response_matrix_fit("test","beam2","None",BI,DI);

    # minus sign needed, to match previous don't divide by 2?
    Response0.Eta_x = 1/(2 * deltae) * (Rxx2 - Rxx1) ;
    Response0.Eta_y = 1/(2 * deltae) * (Ryy2 - Ryy1);

    Response0.Model_R = zeros(N,2);
    
    disp("Multipoles on")
    placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));

    [Rxx, Rxy, Ryx, Ryy, Rxxx,Rxxy, Rxyy, Ryxx, Ryyy] = placet_get_response_matrix_fit("test","beam0","None",BI,DI);

    # overwrite response from file
    Response1.Rxx = Rxx;
    Response1.Ryy = Ryy;

    [Rxx1, Rxy1, Ryx1, Ryy1, Rxxx1, Rxxy1, Rxyy1, Ryxx1, Ryyy1] = placet_get_response_matrix_fit("test","beam1","None",BI,DI);

    [Rxx2, Rxy2, Ryx2, Ryy2, Rxxx2, Rxxy2, Rxyy2, Ryxx2, Ryyy2] = placet_get_response_matrix_fit("test","beam2","None",BI,DI);

    # minus sign needed, to match previous don't divide by 2?
    Response1.Eta_x = 1/(2 * deltae) * (Rxx2 - Rxx1) ;
    Response1.Eta_y = 1/(2 * deltae) * (Ryy2 - Ryy1);

    Response1.Model_R = zeros(N,2);

    save -text testResponse0.dat Response0 Response1
}

source $script_dir/main/dfs_constructmatrices.tcl
