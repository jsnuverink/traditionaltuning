#############################################################################
#
# single shot
#
#############################################################################

# Disable multi-core option 
#ParallelThreads -num 1 

set e_initial 1500
set e0 $e_initial

set script_dir /afs/cern.ch/user/j/jsnuveri/work/traditional/tuning_betas_500_sext_0.7/
if {![file exist $script_dir]} {
    set script_dir [pwd]/..
}

array set args {
    sr 1
    gain1 0.5
    gain2 0.2
    iter1 100
    iter2 40
    sigma 10.0
    bpmres 0.010
    deltae 0.001
    deltakx -1
    deltaky -1
    beta 11
    w1 73
    w2 5
    log 2
    machine 1
    loadmachine 0
    repeat 1
    misalign 0
}

array set args $argv

set gain1 $args(gain1)
set gain2 $args(gain2)
set iter1 $args(iter1)
set iter2 $args(iter2)
set sigma $args(sigma)
set bpmres $args(bpmres)
set deltae $args(deltae)
set deltakx $args(deltakx)
set deltaky $args(deltaky)
set w1 $args(w1)
set w2 $args(w2)
set log $args(log)
set beta $args(beta)
set sr $args(sr)
set machine $args(machine)
set loadmachine $args(loadmachine)
set repeat $args(repeat)
set misalign $args(misalign)

if { $w2 == -1 } {
    set w2 $w1
}

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0
source $script_dir/lattices/trad_ffs_sext_0.7.tcl


if { $log == 2 } {
    TclCall -script {
	array set bm [BeamMeasure]
	set beam_emitt_x $bm(emitt_x)
	set beam_emitt_y $bm(emitt_y)
	set beam_size_x [expr sqrt($bm(sxx) - $bm(x) * $bm(x))]
	set beam_size_y [expr sqrt($bm(syy) - $bm(y) * $bm(y))]
    }
}

Bpm -name IP
Drift -name "D0" -length 3.5
#Drift -name "D0" -length 3.5026092
Bpm
BeamlineSet -name test

array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x  34.142
    beta_y  5.858
}


#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set e0 $e_initial

proc my_survey {} {
    global machine sigma bpmres
    Octave {
	randn("seed", $machine * 1e4);
	BI = placet_get_number_list("test", "bpm");
	DI = placet_get_number_list("test", "dipole");
	QI = placet_get_number_list("test", "quadrupole");
	placet_element_set_attribute("test", DI, "strength_x", 0.0);
	placet_element_set_attribute("test", DI, "strength_y", 0.0);
	placet_element_set_attribute("test", BI, "resolution", $bpmres);
	placet_element_set_attribute("test", BI, "x", randn(size(BI)) * $sigma);
	placet_element_set_attribute("test", BI, "y", randn(size(BI)) * $sigma);
	placet_element_set_attribute("test", QI, "x", randn(size(QI)) * $sigma);
	placet_element_set_attribute("test", QI, "y", randn(size(QI)) * $sigma);
	placet_element_set_attribute("test", MI, "x", randn(size(MI)) * $sigma);
	placet_element_set_attribute("test", MI, "y", randn(size(MI)) * $sigma);
    }
}

if { $log != 2 } {
    set n_slice 1
    set n 1
    set n_total [expr $n_slice*$n]

    make_beam_many beam0 $n_slice $n
    exec echo "$e0 0 0 0 0 0" > particles.in
    BeamRead -file particles.in -beam beam0

} {
    set n_slice 100
    set n 1000
    set n_total [expr $n_slice*$n]

    make_beam_many beam0 $n_slice $n
}

set n_slice 100
set n 1000
set n_total [expr $n_slice*$n]

make_beam_many beam0t $n_slice $n

FirstOrder 1

Octave {
    IP = placet_get_name_number_list("test", "IP");
}

puts "MACHINE: $machine"

Octave {
    if $loadmachine
        if exist("machine_status_$machine.dat", "file")
            disp("reading machine status...");
            load 'machine_status_$machine.dat';
            placet_element_set_attributes("test", STATUS);
        end
    else
        if $misalign
	   # misalign the machine
           disp("Random misalignment errors");
	   my_survey
        else 
            disp("ERROR: machine status file not found");
	    #exit(1)
        end
    end
}

# for GP scripts
source $script_dir/main/multipole_knobs_new.tcl

Octave {
    niter = 20;
    L = zeros(niter,1);
    for i = 1:niter
       [E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
       L(i) = get_lumi(B)
    end
    save -text Lumi.dat L;
    save_beam("electron.ini",B);
    save_beam("positron.ini", B);
}

