Octave {
    Cov_diff = cov(B(:,[ 1 2 3 5 6 ])) - Knobs.BeamCov;
    T_Cov = zeros(15,1);
    index=1;
    for i = 1:5
    for j = i:5
    T_Cov(index++) = Cov_diff(i,j) / (Knobs.BeamStd(i)*Knobs.BeamStd(j));
    end
    end
    Knobs.BeamStd;
    T_Knobs = -pinv(Knobs.K, Knobs.S(6)) * T_Cov
    placet_element_vary_attribute("test", Knobs.I, Knobs.L, T_Knobs);                  
}
