#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set e0 $e_initial
set e2 [expr $e0 * (1.0 + $deltae)]
set e1 [expr $e0 * (1.0 - $deltae)]

puts "e1 = $e1"
puts "e2 = $e2"

puts "Creating beams"

set n_slice 100
set n 1000
set n_total [expr $n_slice*$n]

make_beam_many beam0 $n_slice $n
make_beam_many_energy beam2 $n_slice $n [expr 1. + $deltae ]
make_beam_many_energy beam1 $n_slice $n [expr 1. - $deltae ]

make_beam_many beam0t $n_slice $n
