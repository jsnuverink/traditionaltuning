# Traditional final focus lattice by Hector Garcia

array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x  34.142
    beta_y  5.858
}

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

puts "Load lattice"

SetReferenceEnergy $e0
source $script_dir/lattices/trad_ffs_sext_0.7.tcl

TclCall -script {
    array set bm [BeamMeasure]
    set beam_emitt_x $bm(emitt_x)
    set beam_emitt_y $bm(emitt_y)
    set beam_size_x [expr sqrt($bm(sxx) - $bm(x) * $bm(x))]
    set beam_size_y [expr sqrt($bm(syy) - $bm(y) * $bm(y))]
}

Bpm -name IP
Drift -name "D0" -length 3.5
#Drift -name "D0" -length 3.5026092
Bpm
BeamlineSet -name test
