set e_initial 1500
set e0 $e_initial
set script_dir /afs/cern.ch/user/h/hgarciam/public/CLIC_3TeV_lattices/trad_reopt/tuning_betas_500_sext_0.7

array set args {
    sr 1
}

array set args $argv

set sr $args(sr)

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0

source $script_dir/lattices/trad_ffs_sext_0.7.tcl

Bpm -name IP

BeamlineSet -name test

array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x  34.142
    beta_y  5.858
}


#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set e0 $e_initial
set n_slice 25
set n 1000
set n_total [expr $n_slice*$n]

make_beam_many beam0 $n_slice $n

#if { $sr } {
#  SetReferenceEnergy -beamline test -beam beam0t -survey Zero -iter 5
#}    

source $script_dir/main/multipole_knobs_new.tcl

#Octave {	
#         [E,B]=placet_test_no_correction("test", "beam0", "None");
# 	 S = std(B(:,[ 1 2 3 5 6 ]));
#	 sizeX = S(2)
# 	 sizeY = S(3)
#	 get_lumi(B);
#  	 Lumi_peak = Tcl_GetVar("lumi_peak")
# 	 Lumi_total = Tcl_GetVar("lumi_total")
#}

Octave {
 Table = zeros(1, 10);
}
foreach knob { 1 2 3 4 5 6 7 8 9 10 } {
 Octave {
  row=1;
 }
# foreach step { -10.0 -0.25 0.0 0.25 10.0 } {}
 foreach step { -10.0 -5.0 0.0 5.0 10.0 } {
  Octave {
   L = -test_knob${knob}($step);
   Table(row++, $knob) = L;
  }
 }
}

Octave {
 save -text table.dat Table
}
