Octave {
    R0 = [ 
	  Response0.Rxx Zero
	  Zero Response0.Ryy
	 ];

    ETA0 = [
	    Response0.Eta_x Zero
	    Zero Response0.Eta_y
	   ];

    R1 = [ 
	  Response1.Rxx Zero
	  Zero Response1.Ryy
	 ];

    ETA1 = [
	    Response1.Eta_x Zero
	    Zero Response1.Eta_y
	   ];

    I0 = [
	  diag(ones(1,M)) zeros(M)
	  zeros(M) diag(ones(1,M))
	 ];

    i0 = zeros(2*M,1);
}
