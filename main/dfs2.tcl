puts "Start second DFS"
Octave {
    if $w2 != 0

    #A1 = [ R1 ; $w2 * ETA1 ; $beta * I0 ];

    for i=1:$iter2

    #printf("tracking beam0\n");
    placet_test_no_correction("test", "beam0", "None");

    Log.EmittX(Log.index) += str2num(Tcl_GetVar("beam_emitt_x"));
    Log.EmittY(Log.index) += str2num(Tcl_GetVar("beam_emitt_y"));
    Log.SizeX(Log.index) += str2num(Tcl_GetVar("beam_size_x"));
    Log.SizeY(Log.index) += str2num(Tcl_GetVar("beam_size_y"));

    #printf("measuring dispersion\n");
    Measure1.R = placet_get_bpm_readings("test");
    Measure1.Eta = MeasureDispersion("test", "beam1", "beam2", $deltae);

    b1 = [
	  Measure1.R(:,1) - Response1.Model_R(:,1)
	  Measure1.R(:,2) - Response1.Model_R(:,2)
	 ];

    eta1 = [
	    Measure1.Eta(:,1) - Response1.Model_Eta(:,1)
	    Measure1.Eta(:,2) - Response1.Model_Eta(:,2)
	   ];

    y1 =  [ b1 ; $w2 * eta1 ; $beta * i0 ];
    k1 = -[ R1 ; $w2 * ETA1 ; $beta * I0 ] \ y1;

    placet_element_vary_attribute("test", DI, "strength_x", $gain2 * k1(1:M));
    placet_element_vary_attribute("test", DI, "strength_y", $gain2 * k1((M+1):end));
    
    chi3 = sum(y1 .** 2)
    chi_R = sum(b1 .** 2);
    chi_ETA = sum(eta1 .** 2);

    Log.Chi(Log.index) += chi3;
    Log.Chi_R(Log.index) += chi_R;
    Log.Chi_ETA(Log.index) += chi_ETA;
    Log.Stage(Log.index) = 5;
    Log.index++;
    end
    end

    #disp("Lumi at end second DFS");
    #saveLumi(5);
    STATUS = placet_element_get_attributes("test");
    save -text machine_status_dfs2.dat STATUS;
}
