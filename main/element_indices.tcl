Octave {
    MI = placet_get_number_list("test", "multipole");
    MS = placet_element_get_attribute("test", MI, "strength");

    MI = MI(MS != 0);
    MS = placet_element_get_attribute("test", MI, "strength");
    ML = placet_element_get_attribute("test", MI, "length");
    MT = placet_element_get_attribute("test", MI, "type");

    MRANGEX = zeros(size(MI));
    MRANGEY = zeros(size(MI));
    for multipole = 1:length(MI)
    _ms = abs(MS(multipole));
    _ml = ML(multipole);
    _mt = MT(multipole);
    deltakx = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Sxx(:,multipole)))) / $repeat;
    deltaky = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Syy(:,multipole)))) / $repeat;
    MRANGEX(multipole) = ((deltakx * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
    MRANGEY(multipole) = ((deltaky * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
    end
}
