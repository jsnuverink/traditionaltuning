Octave {
  
  format long
	
  function S = Beamsize(x)

    w1 = exp(x(1));
    w2 = exp(x(2));

    command = sprintf("placet-0.99.06 correct8_scan.tcl bpmres 0.010 w1 %g w2 %g nm 1 log 0 beta 11", w1, w2);
    
    system(command);

    S = load('beamsize.out');
    
    printf("DFS %g %g : %g %g\n", w1, w2, S(1), S(2));

    S = (S(1)/40.0)**2 + (S(2)/1.0)**2;

  end

  x = fmins("Beamsize", [ 0. 0. ]);
  w1 = exp(x(1))
  w2 = exp(x(2))
  S = load('beamsize.out');
  printf("SIZEX : %.15g +/- %.15g\n", S(1), S(3));
  printf("SIZEY : %.15g +/- %.15g\n", S(2), S(4));
}
