Octave {
	function Eta = MeasureDispersion(Beamline, Beam1, Beam2, delta)
		placet_test_no_correction(Beamline, Beam2, "None"); Eta  = placet_get_bpm_readings(Beamline);
		placet_test_no_correction(Beamline, Beam1, "None"); Eta -= placet_get_bpm_readings(Beamline);
		Eta /= 2 * delta;
	end
}

Octave {
	function CB = MeasureCouplingBetabeating(Beamline, Beam, Corrector, Leverage, Kick)
		K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB-=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
		CB/=2*Kick;
	end
}

source $script_dir/main/multipole_knobs_new.tcl

proc align_multipoles {} {
	global bpmres sigma repeat
	Octave {
		BI = placet_get_number_list("test", "bpm");
		placet_element_set_attribute("test", BI, "resolution", 0.0);
	}
	Octave {
		# ms = placet_element_get_attribute("test", MI, "strength");
		pinv_Sxx = pinv(Response0.Sxx);
		pinv_Syy = pinv(Response0.Syy);
		scale = 1;
		# placet_element_set_attribute("test", MI, "strength", 0.0);
		placet_test_no_correction("test", "beam0", "None");
		bpm0 = placet_get_bpm_readings("test");
      		for multipole = [ 1:length(MI) (length(MI)-1):-1:1 ]
			multipole_type = placet_element_get_attribute("test", MI(multipole), "type");
			placet_element_set_attribute("test", MI(multipole), "strength", scale * MS(multipole));
			if multipole_type == 3
				sextupole = MI(multipole);
				if abs(placet_element_get_attribute("test", sextupole, "strength")) > 0
					xs = placet_element_get_attribute("test", sextupole, "x");
					ys = placet_element_get_attribute("test", sextupole, "y");
					mode = 1 ; # 0 == along Y axis, 1 == along X axis, 2 == mixed
					if mode == 0
					  placet_element_set_attribute("test", sextupole, "y", ys + MRANGEY(multipole));
					  DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type-1),1,$repeat);
					  data = zeros(size(DX));
					  index = 1;
					  for dx = DX
						  placet_element_set_attribute("test", sextupole, "x", xs + dx);
						  placet_test_no_correction("test", "beam0", "None");
						  bpm2 = placet_get_bpm_readings("test");
						  dbpm = bpm2 - bpm0;
						  theta_y = pinv_Syy * dbpm(:,2);
						  data(index) += theta_y(multipole);
						  index++;
					  end
					  xoffset = roots(polyfit(DX, data, 1));
					  placet_element_set_attribute("test", sextupole, "x", xs + xoffset + MRANGEX(multipole));
					  DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type-1),1,$repeat);
					  data = zeros(size(DY));
					  index = 1;
					  for dy = DY
						  placet_element_set_attribute("test", sextupole, "y", ys + dy);
              					  placet_test_no_correction("test", "beam0", "None");
						  bpm2 = placet_get_bpm_readings("test");
						  dbpm = bpm2 - bpm0;
						  theta_y = pinv_Syy * dbpm(:,2);
						  data(index) += theta_y(multipole);
						  index++;
					  end
					  yoffset = roots(polyfit(DY, data, 1));
					  placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
					  placet_element_set_attribute("test", sextupole, "y", ys + yoffset);
					elseif mode == 1
						DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
						data = zeros(size(DX));
						index = 1;
						for dx = DX
							placet_element_set_attribute("test", sextupole, "x", xs + dx);			
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_x = pinv_Sxx * dbpm(:,1);
							data(index) += theta_x(multipole);
							index++;
						end
						xoffset = roots(polyderiv(polyfit(DX, data, 2)));
						placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
						DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type),1,$repeat);
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", sextupole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_x = pinv_Sxx * dbpm(:,1);
							data(index) += theta_x(multipole);
							index++;
						end
						yoffset = roots(polyderiv(polyfit(DY, data, 2)));
						placet_element_set_attribute("test", sextupole, "y", ys + yoffset);						
					else # mode == 2
						DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
						data = zeros(size(DX));
						index = 1;
						for dx = DX
							placet_element_set_attribute("test", sextupole, "x", xs + dx);			
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_x = pinv_Sxx * dbpm(:,1);
							data(index) += theta_x(multipole);
							index++;
						end
						xoffset = roots(polyderiv(polyfit(DX, data, 2)));
						placet_element_set_attribute("test", sextupole, "x", xs + xoffset + MRANGEX(multipole));
						DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type-1),1,$repeat);
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", sextupole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_y = pinv_Syy * dbpm(:,2);
							data(index) += theta_y(multipole);
							index++;
						end
						yoffset = roots(polyfit(DY, data, 1));
						placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
						placet_element_set_attribute("test", sextupole, "y", ys + yoffset);
					end

				end
			elseif multipole_type == 4 || multipole_type == 5
				xxxxpole = MI(multipole);
				if abs(placet_element_get_attribute("test", xxxxpole, "strength")) > 0
					xs = placet_element_get_attribute("test", xxxxpole, "x");
					ys = placet_element_get_attribute("test", xxxxpole, "y");
					DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
					data = zeros(size(DX));
					index = 1;
					for dx = DX
						placet_element_set_attribute("test", xxxxpole, "x", xs + dx);
						placet_test_no_correction("test", "beam0", "None");
						bpm2 = placet_get_bpm_readings("test");
						dbpm = bpm2 - bpm0;
						theta_x = pinv_Sxx * dbpm(:,1);
						data(index) += theta_x(multipole);
						index++;
					end
					POL = polyfit(DX, data, multipole_type-1);
							for deriv = 1:(multipole_type-2)
						POL = polyderiv(POL);
					end
					xoffset = roots(POL);
					placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset);
					DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type),1,$repeat);
					if multipole_type == 4
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", xxxxpole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_y = pinv_Syy * dbpm(:,2);
							data(index) += theta_y(multipole);
							index++;
						end
						yoffset = roots(polyderiv(polyderiv(polyfit(DY, data, 3))));
						placet_element_set_attribute("test", xxxxpole, "y", ys + yoffset);
					else
						placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset + MRANGEX(multipole));
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", xxxxpole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_y = pinv_Syy * dbpm(:,2);
							data(index) += theta_y(multipole);
							index++;
						end
						yoffset = roots(polyderiv(polyderiv(polyfit(DY, data, 3))));
						placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset);					
						placet_element_set_attribute("test", xxxxpole, "y", ys + yoffset);
					end		
				end
			end
			# placet_element_set_attribute("test", MI(multipole), "strength", 0.0);
		end
		# placet_element_set_attribute("test", MI, "strength", ms);
	}
	Octave {
		placet_element_set_attribute("test", BI, "resolution", $bpmres);
	}
}


Octave {
	MI = placet_get_number_list("test", "multipole");
	MS = placet_element_get_attribute("test", MI, "strength");

	MI = MI(MS != 0);
	MS = placet_element_get_attribute("test", MI, "strength");
	ML = placet_element_get_attribute("test", MI, "length");
	MT = placet_element_get_attribute("test", MI, "type");

	MRANGEX = zeros(size(MI));
	MRANGEY = zeros(size(MI));
	for multipole = 1:length(MI)
		_ms = abs(MS(multipole));
		_ml = ML(multipole);
		_mt = MT(multipole);
		deltakx = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Sxx(:,multipole)))) / $repeat;
		deltaky = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Syy(:,multipole)))) / $repeat;
		MRANGEX(multipole) = ((deltakx * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
		MRANGEY(multipole) = ((deltaky * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
	end
	
	

}

Octave {
 	DI = placet_get_number_list("test", "dipole");
  
	N = length(placet_get_number_list("test", "bpm"));
	M = length(DI);
	
	Zero = zeros(N,M);
}

proc my_survey {} {
	global machine sigma bpmres
	Octave {
		randn("seed", $machine * 1e4);
		BI = placet_get_number_list("test", "bpm");
		DI = placet_get_number_list("test", "dipole");
		QI = placet_get_number_list("test", "quadrupole");
		placet_element_set_attribute("test", DI, "strength_x", 0.0);
		placet_element_set_attribute("test", DI, "strength_y", 0.0);
		placet_element_set_attribute("test", BI, "resolution", $bpmres);
		placet_element_set_attribute("test", BI, "x", randn(size(BI)) * $sigma);
		placet_element_set_attribute("test", BI, "y", randn(size(BI)) * $sigma);
		placet_element_set_attribute("test", QI, "x", randn(size(QI)) * $sigma);
		placet_element_set_attribute("test", QI, "y", randn(size(QI)) * $sigma);
		placet_element_set_attribute("test", MI, "x", randn(size(MI)) * $sigma);
		placet_element_set_attribute("test", MI, "y", randn(size(MI)) * $sigma);
	}
}

Octave {
	emittH = zeros($nm, 7);
	emittV = zeros($nm, 7);
	sizeH = zeros($nm, 7);
	sizeV = zeros($nm, 7);
	Lumi = zeros($nm, 7);
	chi1=zeros($nm, 1);
	IP = placet_get_name_number_list("test", "IP");
}

Octave {
	global Log;
	Log.Chi = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_R = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_ETA = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_CB1 = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_CB2 = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.EmittX = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.EmittY = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.SizeX = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.SizeY = zeros(4 * ($iter1 + $iter2) + 4, 1);
}


