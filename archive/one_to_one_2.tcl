# separate bx and by weigths
puts "One to One"

Octave {
    R0 = [ 
      Response0.Rxx Zero
      Zero Response0.Ryy
    ];

    I0x = [
      diag(ones(1,M)) zeros(M)
    ];

    I0y = [
      zeros(M) diag(ones(1,M))
    ];

    I0 = [     
      diag(ones(1,M)) zeros(M)
      zeros(M) diag(ones(1,M))
    ];

    i0 = zeros(2*M,1);
	}

  Octave {
	# 1:1 correction
    	placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));
		K0 = [ R0 ; $betax * I0x; $betay * I0y ];

		for i=1:$iter1
			# tracking
 			placet_test_no_correction("test", "beam0", "None");
			# BPM reading
			Measure0.R = placet_get_bpm_readings("test");
			# Differential orbit
			b0 = [
				Measure0.R(:,1) - Response0.Model_R(:,1)
				Measure0.R(:,2) - Response0.Model_R(:,2)
			];
			# Calculating correction based on model
			k0 = -[ R0 ; $betax * I0x; $betay * I0y ] \ [ b0 ; i0 ];
			# Apply correction
			placet_element_vary_attribute("test", DI, "strength_x", $gain1 * k0(1:M));
			placet_element_vary_attribute("test", DI, "strength_y", $gain1 * k0((M+1):end));
			# Chi test
			#if $log
			chi1 = sum(b0 .** 2)+$betax**2*sum(k0(1:M).**2)+$betay**2*sum(k0((M+1):end).**2)
			#end
		end

                placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));
		}
 	
        # end of 1:1
	puts "End of One to One"
