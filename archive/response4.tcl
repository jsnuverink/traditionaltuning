ParallelThreads -num 1

set e_initial 1500
set e0 $e_initial
set script_dir /afs/cern.ch/user/h/hgarciam/public/CLIC_3TeV_lattices/trad_reopt/tuning_betas_500_sext_0.7

array set args {
    deltae 0.001
    deltak 0.001
    sr 1
}

array set args $argv

set deltae $args(deltae)
set deltak $args(deltak)
set sr $args(sr)

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0

source $script_dir/lattices/trad_ffs_sext_0.7.tcl

Bpm -name "IP"
Drift -name "D5" -length 3.5
Bpm
BeamlineSet -name test

array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x  34.142
    beta_y  5.858
}

#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0


set n_slice 1
set n 1
set n_total [expr $n_slice*$n]

set e0 $e_initial
set e2 [expr $e0 * (1.0 + $deltae)]
set e1 [expr $e0 * (1.0 - $deltae)]

puts "e1 = $e1"
puts "e2 = $e2"

make_beam_many beam0 $n_slice $n
exec echo "$e0 0 0 0 0 0" > particles.in
BeamRead -file particles.in -beam beam0

make_beam_many beam2 $n_slice $n
exec echo "$e2 0 0 0 0 0" > particles.in
BeamRead -file particles.in -beam beam2

make_beam_many beam1 $n_slice $n
exec echo "$e1 0 0 0 0 0" > particles.in
BeamRead -file particles.in -beam beam1

FirstOrder 1

if { $sr } {
    Octave {
    	SI = placet_get_number_list("test", "sbend");
	placet_element_set_attribute("test", SI, "thin_lens", int32(100));
    }
}

#if { $sr } {
#    SetReferenceEnergy -beamline test -beam beam0 -survey Zero -iter 5
#}

Octave {
	function R = Get_ResponseMatrix(Beamline, Beam, Correctors, Leverage, Kick, Axis)
		N = length(placet_get_number_list(Beamline, "bpm"));
		M = length(Correctors);
		R = zeros(N,M);
		for i=1:M
			Corrector=Correctors(i);
			K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
			placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
			placet_test_no_correction(Beamline, Beam, "None");
			B=placet_get_bpm_readings(Beamline);
			if Axis == "X" || Axis == "x"
				R(:,i) = B(:,1);
			else
				R(:,i) = B(:,2);
			end
			placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
			placet_test_no_correction(Beamline, Beam, "None");
			B=placet_get_bpm_readings(Beamline);
			if Axis == "X" || Axis == "x"
				R(:,i) -= B(:,1);
			else
				R(:,i) -= B(:,2);
			end
			placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
		end
		R/=2*Kick;
	end
}

Octave {
	function Eta = MeasureDispersion(Beamline, Beam1, Beam2, delta)
		placet_test_no_correction(Beamline, Beam2, "None"); Eta  = placet_get_bpm_readings(Beamline);
		placet_test_no_correction(Beamline, Beam1, "None"); Eta -= placet_get_bpm_readings(Beamline);
 		Eta /= 2 * delta;
	end
}

Octave {
	function D = Get_DispersionResponseMatrix(Beamline, Beam1, Beam2, Correctors, Leverage, Kick, Axis)
		N = length(placet_get_number_list(Beamline, "bpm"));
		M = length(Correctors);
		S = zeros(N,M);
		for i=1:M
			Corrector=Correctors(i);
			K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
			placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
			Eta = MeasureDispersion(Beamline, Beam1, Beam2, $deltae);
			if Axis == "X" || Axis == "x"
				D(:,i) = Eta(:,1);
			else
				D(:,i) = Eta(:,2);
			end
			placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
			Eta = MeasureDispersion(Beamline, Beam1, Beam2, $deltae);
			if Axis == "X" || Axis == "x"
				D(:,i) -= Eta(:,1);
			else
				D(:,i) -= Eta(:,2);
			end
			placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
		end
		D/=2*Kick;
	end
}

Octave {
	function CB = MeasureCouplingBetabeating(Beamline, Beam, Corrector, Leverage, Kick)
		K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB-=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
		CB/=2*Kick;
	end
}

Octave {
	function CB = Get_CB(Beamline, Beam, Corrector, Kick)
		Cx = MeasureCouplingBetabeating(Beamline, Beam, Corrector, "x", Kick);
		Cy = MeasureCouplingBetabeating(Beamline, Beam, Corrector, "y", Kick);
		CB.Bx = Cx(:,1);	
		CB.By = Cy(:,2);
		CB.Cx = Cy(:,1);
		CB.Cy = Cx(:,2);	
	end
}

Octave {
	function Response = Get_CB_ResponseMatrix(Beamline, Beam, Correctors, Kick, CM_Corrector)
		N = length(placet_get_number_list(Beamline, "bpm"));
		M = length(Correctors);
		Response.Bxx = zeros(N,M);
		Response.Byx = zeros(N,M);
		Response.Cxy = zeros(N,M);
		Response.Cyy = zeros(N,M);
		for i=1:M
			Corrector = Correctors(i);
			K0 = placet_element_get_attribute(Beamline, Corrector, "x");
			placet_element_set_attribute(Beamline, Corrector, "x", K0+Kick);
			CB2 = Get_CB(Beamline, Beam, CM_Corrector, Kick);
			placet_element_set_attribute(Beamline, Corrector, "x", K0-Kick);
			CB1 = Get_CB(Beamline, Beam, CM_Corrector, Kick);
			placet_element_set_attribute(Beamline, Corrector, "x", K0);
			Response.Bxx(:,i) = (CB2.Bx - CB1.Bx) / 2 / Kick;
			Response.Byx(:,i) = (CB2.By - CB1.By) / 2 / Kick;
			K0 = placet_element_get_attribute(Beamline, Corrector, "y");
			placet_element_set_attribute(Beamline, Corrector, "y", K0+Kick);
			CB2 = Get_CB(Beamline, Beam, CM_Corrector, Kick);
			placet_element_set_attribute(Beamline, Corrector, "y", K0-Kick);
			CB1 = Get_CB(Beamline, Beam, CM_Corrector, Kick);
			placet_element_set_attribute(Beamline, Corrector, "y", K0);
			Response.Cxy(:,i) = (CB2.Cx - CB1.Cx) / 2 / Kick;
			Response.Cyy(:,i) = (CB2.Cy - CB1.Cy) / 2 / Kick;
		end
	end
}

Octave {
	function R = Get_ResponseMatrix_Optics(beamline, B, C, Axis)
		if nargin==4 && ischar(beamline) && isvector(B) && isvector(C)
			R=zeros(length(B),length(C));
			for i=1:length(C)
				J=find(C(i)<B);
				if length(J)>0
					Ei=placet_element_get_attribute(beamline, C(i), "e0");
					for j=1:length(J)
						T=placet_get_transfer_matrix(beamline, C(i), B(J(j))-1);
						Ej=placet_element_get_attribute(beamline, B(J(j)), "e0");
						if Axis == "X" || Axis == "x"
							R(J(j),i)=T(1,2)/sqrt(Ei*Ej);
						else
							R(J(j),i)=T(3,4)/sqrt(Ei*Ej);
						end
					end
				end
			end
		endif
	endfunction
}

Octave {
	MI = placet_get_number_list("test", "multipole");
	MS = placet_element_get_attribute("test", MI, "strength");
	MI = MI(MS != 0);
	MS = placet_element_get_attribute("test", MI, "strength");
}

Octave {
 	DI = placet_get_number_list("test", "dipole");
 	DS = placet_element_get_attribute("test", DI, "s");
	dk = $deltak;
}

Zero

Octave {
	BI = placet_get_number_list("test", "bpm");
 	placet_element_set_attribute("test", BI, "resolution", 0.0);
}

foreach use_multipole { 0 1 } {
  puts "MULTIPOLE: $use_multipole/1"
  Octave {
    if $use_multipole==0
      placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));
    else
      placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));
    end
    placet_test_no_correction("test", "beam0", "None");
    Response${use_multipole}.Model_R = placet_get_bpm_readings("test");
    Response${use_multipole}.Rxx = Get_ResponseMatrix("test", "beam0", DI, "strength_x", dk, "x");
    Response${use_multipole}.Ryy = Get_ResponseMatrix("test", "beam0", DI, "strength_y", dk, "y");
    Response${use_multipole}.Model_Eta = MeasureDispersion("test", "beam1", "beam2", $deltae);
    Response${use_multipole}.Eta_x = Get_DispersionResponseMatrix("test", "beam1", "beam2", DI, "strength_x", dk, "x");
    Response${use_multipole}.Eta_y = Get_DispersionResponseMatrix("test", "beam1", "beam2", DI, "strength_y", dk, "y");
    if $use_multipole == 0
      Response${use_multipole}.Sxx = Get_ResponseMatrix_Optics("test", BI, MI, "x");
      Response${use_multipole}.Syy = Get_ResponseMatrix_Optics("test", BI, MI, "y");
    else
      Response${use_multipole}.Rxy = Get_ResponseMatrix("test", "beam0", DI, "strength_y", dk, "x");
      Response${use_multipole}.Ryx = Get_ResponseMatrix("test", "beam0", DI, "strength_x", dk, "y");
      Response${use_multipole}.Eta_xy = Get_DispersionResponseMatrix("test", "beam1", "beam2", DI, "strength_y", dk, "x");
      Response${use_multipole}.Eta_yx = Get_DispersionResponseMatrix("test", "beam1", "beam2", DI, "strength_x", dk, "y");
    end
  }
}

##### Reference Trajectory and Dispersion

if { $sr } {
  set n_slice 100
  set n 1000
  set n_total [expr $n_slice*$n]
  make_beam_many beam0t $n_slice $n
  make_beam_many_energy beam2t $n_slice $n [expr 1. + $deltae ]
  make_beam_many_energy beam1t $n_slice $n [expr 1. - $deltae ]
  # SetReferenceEnergy -beamline test -beam beam0t -survey Zero -iter 5
  foreach use_multipole { 0 1 } {
    puts "MULTIPOLE: $use_multipole/1"
    Octave {
      if $use_multipole == 0
        placet_element_set_attribute("test", MI, "strength", complex(0.0,0.0));
      else
        placet_element_set_attribute("test", MI, "strength", complex(MS,0.0));
      end
      placet_test_no_correction("test", "beam0t", "None");
      Response${use_multipole}.Model_R = placet_get_bpm_readings("test");
      Response${use_multipole}.Model_Eta = MeasureDispersion("test", "beam1t", "beam2t", $deltae);
    }
  }
  Octave {
    IP = placet_get_name_number_list("test", "IP");
    [E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
    sizeX = std(B(:,2))
    sizeY = std(B(:,3))
  }
}

Octave {
  save -text Response4_sr_${sr}_deltae_${deltae}.dat Response0 Response1
}
