# comment out get_lumi when log == 0
# comment out tracking of beam0t, when log == 0, except for last tracking

#################################################################################################################################
#                                                                                                                                
# correct5.tcl
#
# it uses my knobs
#                                                                                                                                
#################################################################################################################################

set e_initial 250
set e0 $e_initial
set script_dir ~/public/CLIC_low_energy_lattices/tuning/

array set args {
	nm 1
	sr 0
	gain1 0.5
	gain2 0.2
	iter1 100
	iter2 100
	sigma 10.0
	bpmres 0.010
	deltae 0.001
	deltakx -1
	deltaky -1
	beta 10
	w1 635
	w2 11
	sv -1
	log 0
	machine all
	repeat 1
}

array set args $argv

set nm $args(nm)
set gain1 $args(gain1)
set gain2 $args(gain2)
set iter1 $args(iter1)
set iter2 $args(iter2)
set sigma $args(sigma)
set bpmres $args(bpmres)
set deltae $args(deltae)
set deltakx $args(deltakx)
set deltaky $args(deltaky)
set w1 $args(w1)
set w2 $args(w2)
set sv $args(sv)
set log $args(log)
set beta $args(beta)
set sr $args(sr)
set machine $args(machine)
set repeat $args(repeat)
  
if { $w2 == -1 } {
	set w2 $w1
}
  
set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0
source /afs/cern.ch/user/h/hgarciam/public/CLIC_low_energy_lattices/tuning/lattices/twissout_500GeV.tcl
#source $script_dir/lattices/bds.match.linac4b_v_10_10_11

if { $log == 2 } {
	TclCall -script {
		array set bm [BeamMeasure]
		set beam_emitt_x $bm(emitt_x)
		set beam_emitt_y $bm(emitt_y)
		set beam_size_x [expr sqrt($bm(sxx) - $bm(x) * $bm(x))]
		set beam_size_y [expr sqrt($bm(syy) - $bm(y) * $bm(y))]
	}
}
#Bpm -name IP
#Drift -name "D0" -length 3.5
#Drift -name "D0" -length 3.5026092
#Bpm
BeamlineSet -name test

# Traditional FFS match

array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x 34.142
    beta_y 5.858
}

#Nominal BDS

#array set match {
#    alpha_x 0.00
#    alpha_y 0.00
#      beta_x 33.07266007	# Nominal 500
#      beta_y 8.962361942
#}

#emittances unit is e-7
set match(emitt_x)  24.0
set match(emitt_y)  0.25
#set match(charge) 4.0e9
set match(charge) 6.8e9
set charge $match(charge)
set match(sigma_z) 50.0
set match(phase) 0.0
set match(e_spread) -1.0

set e0 $e_initial
set e2 [expr $e0 * (1.0 + $deltae)]
set e1 [expr $e0 * (1.0 - $deltae)]

puts "e1 = $e1"
puts "e2 = $e2"

if { $log != 2 } {
	set n_slice 1
	set n 1
	set n_total [expr $n_slice*$n]

	make_beam_many beam0 $n_slice $n
	exec echo "$e0 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam0

	make_beam_many beam2 $n_slice $n
	exec echo "$e2 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam2

	make_beam_many beam1 $n_slice $n
	exec echo "$e1 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam1
} {
	set n_slice 20
	set n 100
	set n_total [expr $n_slice*$n]

	make_beam_many beam0 $n_slice $n
 	make_beam_many_energy beam2 $n_slice $n [expr 1. + $deltae ]
	make_beam_many_energy beam1 $n_slice $n [expr 1. - $deltae ]
}

set n_slice 20
set n 100
set n_total [expr $n_slice*$n]

make_beam_many beam0t $n_slice $n

FirstOrder 1

#if { $sr } {
#	SetReferenceEnergy -beamline test -beam beam0t -survey Zero -iter 5
#} else {
#	SetReferenceEnergy -beamline test -beam beam0 -survey Zero -iter 1
#}


Octave {
	function Eta = MeasureDispersion(Beamline, Beam1, Beam2, delta)
		placet_test_no_correction(Beamline, Beam2, "None"); Eta  = placet_get_bpm_readings(Beamline);
		placet_test_no_correction(Beamline, Beam1, "None"); Eta -= placet_get_bpm_readings(Beamline);
		Eta /= 2 * delta;
	end
}

Octave {
	function CB = MeasureCouplingBetabeating(Beamline, Beam, Corrector, Leverage, Kick)
		K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB-=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
		CB/=2*Kick;
	end
}

source $script_dir/main/multipole_knobs_new.tcl

proc align_multipoles {} {
	global bpmres sigma repeat
	Octave {
		BI = placet_get_number_list("test", "bpm");
		placet_element_set_attribute("test", BI, "resolution", 0.0);
	}
	Octave {
		# ms = placet_element_get_attribute("test", MI, "strength");
		pinv_Sxx = pinv(Response0.Sxx);
		pinv_Syy = pinv(Response0.Syy);
		scale = 1;
		# placet_element_set_attribute("test", MI, "strength", 0.0);
		placet_test_no_correction("test", "beam0", "None");
		bpm0 = placet_get_bpm_readings("test");
      		for multipole = [ 1:length(MI) (length(MI)-1):-1:1 ]
			multipole_type = placet_element_get_attribute("test", MI(multipole), "type");
			placet_element_set_attribute("test", MI(multipole), "strength", scale * MS(multipole));
			if multipole_type == 3
				sextupole = MI(multipole);
				if abs(placet_element_get_attribute("test", sextupole, "strength")) > 0
					xs = placet_element_get_attribute("test", sextupole, "x");
					ys = placet_element_get_attribute("test", sextupole, "y");
					if xs>ys
						mode=0
					elseif mode=1
					end
					mode = 1 ; # 0 == along Y axis, 1 == along X axis, 2 == mixed
					if mode == 0
					  placet_element_set_attribute("test", sextupole, "y", ys + MRANGEY(multipole));
					  DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type-1),1,$repeat);
					  data = zeros(size(DX));
					  index = 1;
					  for dx = DX
						  placet_element_set_attribute("test", sextupole, "x", xs + dx);
						  placet_test_no_correction("test", "beam0", "None");
						  bpm2 = placet_get_bpm_readings("test");
						  dbpm = bpm2 - bpm0;
						  theta_y = pinv_Syy * dbpm(:,2);
						  data(index) += theta_y(multipole);
						  index++;
					  end
					  xoffset = roots(polyfit(DX, data, 1));
					  placet_element_set_attribute("test", sextupole, "x", xs + xoffset + MRANGEX(multipole));
					  DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type-1),1,$repeat);
					  data = zeros(size(DY));
					  index = 1;
					  for dy = DY
						  placet_element_set_attribute("test", sextupole, "y", ys + dy);
              					  placet_test_no_correction("test", "beam0", "None");
						  bpm2 = placet_get_bpm_readings("test");
						  dbpm = bpm2 - bpm0;
						  theta_y = pinv_Syy * dbpm(:,2);
						  data(index) += theta_y(multipole);
						  index++;
					  end
					  yoffset = roots(polyfit(DY, data, 1));
					  placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
					  placet_element_set_attribute("test", sextupole, "y", ys + yoffset);
					elseif mode == 1
						DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
						data = zeros(size(DX));
						index = 1;
						for dx = DX
							placet_element_set_attribute("test", sextupole, "x", xs + dx);			
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_x = pinv_Sxx * dbpm(:,1);
							data(index) += theta_x(multipole);
							index++;
						end
						xoffset = roots(polyderiv(polyfit(DX, data, 2)));
						placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
						DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type),1,$repeat);
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", sextupole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_x = pinv_Sxx * dbpm(:,1);
							data(index) += theta_x(multipole);
							index++;
						end
						yoffset = roots(polyderiv(polyfit(DY, data, 2)));
						placet_element_set_attribute("test", sextupole, "y", ys + yoffset);						
					else # mode == 2
						DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
						data = zeros(size(DX));
						index = 1;
						for dx = DX
							placet_element_set_attribute("test", sextupole, "x", xs + dx);			
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_x = pinv_Sxx * dbpm(:,1);
							data(index) += theta_x(multipole);
							index++;
						end
						xoffset = roots(polyderiv(polyfit(DX, data, 2)));
						placet_element_set_attribute("test", sextupole, "x", xs + xoffset + MRANGEX(multipole));
						DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type-1),1,$repeat);
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", sextupole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_y = pinv_Syy * dbpm(:,2);
							data(index) += theta_y(multipole);
							index++;
						end
						yoffset = roots(polyfit(DY, data, 1));
						placet_element_set_attribute("test", sextupole, "x", xs + xoffset);
						placet_element_set_attribute("test", sextupole, "y", ys + yoffset);
					end

				end
			elseif multipole_type == 4 || multipole_type == 5
				xxxxpole = MI(multipole);
				if abs(placet_element_get_attribute("test", xxxxpole, "strength")) > 0
					xs = placet_element_get_attribute("test", xxxxpole, "x");
					ys = placet_element_get_attribute("test", xxxxpole, "y");
					DX = repmat(linspace(-MRANGEX(multipole), MRANGEX(multipole), multipole_type),1,$repeat);
					data = zeros(size(DX));
					index = 1;
					for dx = DX
						placet_element_set_attribute("test", xxxxpole, "x", xs + dx);
						placet_test_no_correction("test", "beam0", "None");
						bpm2 = placet_get_bpm_readings("test");
						dbpm = bpm2 - bpm0;
						theta_x = pinv_Sxx * dbpm(:,1);
						data(index) += theta_x(multipole);
						index++;
					end
					POL = polyfit(DX, data, multipole_type-1);
							for deriv = 1:(multipole_type-2)
						POL = polyderiv(POL);
					end
					xoffset = roots(POL);
					placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset);
					DY = repmat(linspace(-MRANGEY(multipole), MRANGEY(multipole), multipole_type),1,$repeat);
					if multipole_type == 4
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", xxxxpole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_y = pinv_Syy * dbpm(:,2);
							data(index) += theta_y(multipole);
							index++;
						end
						yoffset = roots(polyderiv(polyderiv(polyfit(DY, data, 3))));
						placet_element_set_attribute("test", xxxxpole, "y", ys + yoffset);
					else
						placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset + MRANGEX(multipole));
						data = zeros(size(DY));
						index = 1;
						for dy = DY
							placet_element_set_attribute("test", xxxxpole, "y", ys + dy);
							placet_test_no_correction("test", "beam0", "None");
							bpm2 = placet_get_bpm_readings("test");
							dbpm = bpm2 - bpm0;
							theta_y = pinv_Syy * dbpm(:,2);
							data(index) += theta_y(multipole);
							index++;
						end
						yoffset = roots(polyderiv(polyderiv(polyfit(DY, data, 3))));
						placet_element_set_attribute("test", xxxxpole, "x", xs + xoffset);					
						placet_element_set_attribute("test", xxxxpole, "y", ys + yoffset);
					end		
				end
			end
			# placet_element_set_attribute("test", MI(multipole), "strength", 0.0);
		end
		# placet_element_set_attribute("test", MI, "strength", ms);
	}
	Octave {
		placet_element_set_attribute("test", BI, "resolution", $bpmres);
	}
}

Octave {
	load '$script_dir/matrices/Response4_sr_${sr}_deltae_${deltae}.dat';
}

Octave {
	MI = placet_get_number_list("test", "multipole");
	MS = placet_element_get_attribute("test", MI, "strength");

	MI = MI(MS != 0);
	MS = placet_element_get_attribute("test", MI, "strength");
	ML = placet_element_get_attribute("test", MI, "length");
	MT = placet_element_get_attribute("test", MI, "type");

	MRANGEX = zeros(size(MI));
	MRANGEY = zeros(size(MI));
	for multipole = 1:length(MI)
		_ms = abs(MS(multipole));
		_ml = ML(multipole);
		_mt = MT(multipole);
		deltakx = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Sxx(:,multipole)))) / $repeat;
		deltaky = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Syy(:,multipole)))) / $repeat;
		MRANGEX(multipole) = ((deltakx * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
		MRANGEY(multipole) = ((deltaky * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
	end
	
	

}

Octave {
 	DI = placet_get_number_list("test", "dipole");
  
	N = length(placet_get_number_list("test", "bpm"));
	M = length(DI);
	
	Zero = zeros(N,M);
}

proc my_survey {} {
	global machine sigma bpmres
	Octave {
		randn("seed", $machine * 1e4);
		BI = placet_get_number_list("test", "bpm");
		DI = placet_get_number_list("test", "dipole");
		QI = placet_get_number_list("test", "quadrupole");
		placet_element_set_attribute("test", DI, "strength_x", 0.0);
		placet_element_set_attribute("test", DI, "strength_y", 0.0);
		placet_element_set_attribute("test", BI, "resolution", $bpmres);
		placet_element_set_attribute("test", BI, "x", randn(size(BI)) * $sigma);
		placet_element_set_attribute("test", BI, "y", randn(size(BI)) * $sigma);
		placet_element_set_attribute("test", QI, "x", randn(size(QI)) * $sigma);
		placet_element_set_attribute("test", QI, "y", randn(size(QI)) * $sigma);
		placet_element_set_attribute("test", MI, "x", randn(size(MI)) * $sigma);
		placet_element_set_attribute("test", MI, "y", randn(size(MI)) * $sigma);
	}
}

Octave {
	emittH = zeros($nm, 7);
	emittV = zeros($nm, 7);
	sizeH = zeros($nm, 7);
	sizeV = zeros($nm, 7);
	Lumi = zeros($nm, 7);
	IP = placet_get_name_number_list("test", "IP");
}

Octave {
	global Log;
	Log.Chi = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_R = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_ETA = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_CB1 = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_CB2 = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.EmittX = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.EmittY = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.SizeX = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.SizeY = zeros(4 * ($iter1 + $iter2) + 4, 1);
}

if { $machine == "all" } {
	set machine_start 1
	set machine_end	$nm
} {
	set machine_start $machine
	set machine_end	$machine
}

for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {
	
	puts "MACHINE: $machine/$nm"
	
 	Octave {
		Log.index = 1;
	}

	Octave {
    R0 = [ 
      Response0.Rxx Zero
      Zero Response0.Ryy
    ];

    ETA0 = [
      Response0.Eta_x Zero
      Zero Response0.Eta_y
    ];

    R1 = [ 
      Response1.Rxx Zero
      Zero Response1.Ryy
    ];

    ETA1 = [
      Response1.Eta_x Zero
      Zero Response1.Eta_y
    ];

    I0 = [
      diag(ones(1,M)) zeros(M)
      zeros(M) diag(ones(1,M))
    ];

    i0 = zeros(2*M,1);
	}

set skip 0

#Octave {
  #if exist("machine_status.dat", "file")
    #disp("reading machine status...");
    #load 'machine_status.dat';
    #placet_element_set_attributes("test", STATUS);
    #Tcl_SetVar("skip", 1);
  #end
#}
     
	# misalign the machine
	#puts "Random misalignment errors"
	my_survey

  
	Octave {
		Lumi
		[E,B] = placet_test_no_correction("test", "beam0", "None",1,0,IP);
		Lumi($machine,1) = get_lumi(B)

	placet_element_set_attribute("test", MI, "strength", 0.0);

	# 1:1 correction

		K0 = [ R0 ; $beta * I0 ];


		for i=1:$iter1

 			placet_test_no_correction("test", "beam0", "None");
			
			Measure0.R = placet_get_bpm_readings("test");

			b0 = [
				Measure0.R(:,1) - Response0.Model_R(:,1)
				Measure0.R(:,2) - Response0.Model_R(:,2)
			];

			k0 = -[ R0 ; $beta * I0 ] \ [ b0 ; i0 ];

			placet_element_vary_attribute("test", DI, "strength_x", $gain1 * k0(1:M));
			placet_element_vary_attribute("test", DI, "strength_y", $gain1 * k0((M+1):end));

			if $log
				chi1 = sum(b0 .** 2)
			end
		end

		placet_element_set_attribute("test", MI, "strength", MS);
		[E,B] = placet_test_no_correction("test", "beam0", "None",1,0,IP);
		Lumi($machine,2) = get_lumi(B), $machine
		#placet_element_set_attribute("test", MI, "strength", 0.0);
	}

  Octave {
  STATUS = placet_element_get_attributes("test");
  save -text machine_status.dat STATUS Lumi
	}        

Octave {
machine=1
  if exist("machine_status.dat", "file")
    disp("reading machine status...");
    load 'machine_status.dat';
    placet_element_set_attributes("test", STATUS);
    #Tcl_SetVar("skip", 1);
  end
}

Octave {
		[E,B] = placet_test_no_correction("test", "beam0", "None",1,0,IP);
		Lumi($machine,3) = get_lumi(B)
		
  STATUS = placet_element_get_attributes("test");
  save -text machine_status_new.dat STATUS Lumi
	}

}




