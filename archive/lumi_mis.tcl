# comment out get_lumi when log == 0
# comment out tracking of beam0t, when log == 0, except for last tracking

####################################################################################################
#                                                                                                                                
# correct5.tcl
#
#                                                                                                                                
#####################################################################################################

set e_initial 250
set e0 $e_initial
set script_dir /afs/cern.ch/user/h/hgarciam/private/tuning_trad_500

array set args {
	nm 1
	sr 1
	gain1 0.5
	gain2 0.2
	iter1 100
	iter2 100
	sigma 10.0
	bpmres 0.010
	deltae 0.001
	deltakx -1
	deltaky -1
	beta 4
	w1 4
	w2 1
	sv -1
	log 0
	machine all
	repeat 1
}

array set args $argv

set nm $args(nm)
set gain1 $args(gain1)
set gain2 $args(gain2)
set iter1 $args(iter1)
set iter2 $args(iter2)
set sigma $args(sigma)
set bpmres $args(bpmres)
set deltae $args(deltae)
set deltakx $args(deltakx)
set deltaky $args(deltaky)
set w1 $args(w1)
set w2 $args(w2)
set sv $args(sv)
set log $args(log)
set beta $args(beta)
set sr $args(sr)
set machine $args(machine)
set repeat $args(repeat)
  
if { $w2 == -1 } {
	set w2 $w1
}
  
set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0
source $script_dir/lattices/ffs_trad_bx8_by0.1.tcl


if { $log == 2 } {
	TclCall -script {
		array set bm [BeamMeasure]
		set beam_emitt_x $bm(emitt_x)
		set beam_emitt_y $bm(emitt_y)
		set beam_size_x [expr sqrt($bm(sxx) - $bm(x) * $bm(x))]
		set beam_size_y [expr sqrt($bm(syy) - $bm(y) * $bm(y))]
	}
}

Bpm -name IP
#Drift -name "D0" -length 3.5
#Drift -name "D0" -length 3.5026092
#Bpm
BeamlineSet -name test

array set match {
    alpha_x 0.00
    alpha_y 0.00
    beta_x 42.678
    beta_y 7.322
}

#emittances unit is e-7
set match(emitt_x)  24.0
set match(emitt_y)  0.25
#set match(charge) 4.0e9
set match(charge) 6.8e9
set charge $match(charge)
set match(sigma_z) 72.0
set match(phase) 0.0
set match(e_spread) -1.0

set e0 $e_initial

if { $log != 2 } {
	set n_slice 1
	set n 1
	set n_total [expr $n_slice*$n]

	make_beam_many beam0 $n_slice $n
	exec echo "$e0 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam0

	make_beam_many beam2 $n_slice $n
	exec echo "$e2 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam2

	make_beam_many beam1 $n_slice $n
	exec echo "$e1 0 0 0 0 0" > particles.in
	BeamRead -file particles.in -beam beam1
} {
	set n_slice 1
	set n 1
	set n_total [expr $n_slice*$n]

	make_beam_many beam0 $n_slice $n
 	make_beam_many_energy beam2 $n_slice $n [expr 1. + $deltae ]
	make_beam_many_energy beam1 $n_slice $n [expr 1. - $deltae ]
}

set n_slice 100
set n 1000
set n_total [expr $n_slice*$n]

make_beam_many beam0t $n_slice $n

FirstOrder 1

#if { $sr } {
#	SetReferenceEnergy -beamline test -beam beam0t -survey Zero -iter 5
#} else {
#	SetReferenceEnergy -beamline test -beam beam0 -survey Zero -iter 1
#}


Octave {
	function Eta = MeasureDispersion(Beamline, Beam1, Beam2, delta)
		placet_test_no_correction(Beamline, Beam2, "None"); Eta  = placet_get_bpm_readings(Beamline);
		placet_test_no_correction(Beamline, Beam1, "None"); Eta -= placet_get_bpm_readings(Beamline);
		Eta /= 2 * delta;
	end
}

Octave {
	function CB = MeasureCouplingBetabeating(Beamline, Beam, Corrector, Leverage, Kick)
		K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
		placet_test_no_correction(Beamline, Beam, "None");
		CB-=placet_get_bpm_readings(Beamline);
		placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
		CB/=2*Kick;
	end
}

source $script_dir/main/multipole_knobs_new.tcl
source align_multipoles_new.tcl

Octave {
	load '$script_dir/matrices/Response4_sr_${sr}_deltae_${deltae}.dat';
}

Octave {
	MI = placet_get_number_list("test", "multipole");
	MS = placet_element_get_attribute("test", MI, "strength");

	MI = MI(MS != 0);
	MS = placet_element_get_attribute("test", MI, "strength");
	ML = placet_element_get_attribute("test", MI, "length");
	MT = placet_element_get_attribute("test", MI, "type");

	MRANGEX = zeros(size(MI));
	MRANGEY = zeros(size(MI));
	for multipole = 1:length(MI)
		_ms = abs(MS(multipole));
		_ml = ML(multipole);
		_mt = MT(multipole);
		deltakx = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Sxx(:,multipole)))) / $repeat;
		deltaky = 3 * sqrt(2) * $bpmres * max(abs(pinv(Response0.Syy(:,multipole)))) / $repeat;
		MRANGEX(multipole) = ((deltakx * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
		MRANGEY(multipole) = ((deltaky * 1e-6) * $e0 * factorial(_mt-1) / _ms ) ** (1 / (_mt - 1)) * 1e6;
	end
	
}

Octave {
 	DI = placet_get_number_list("test", "dipole");
  
	N = length(placet_get_number_list("test", "bpm"));
	M = length(DI);
	
	Zero = zeros(N,M);
}

proc my_survey {} {
	global machine sigma bpmres
	Octave {
		randn("seed", $machine * 1e4);
		BI = placet_get_number_list("test", "bpm");
		DI = placet_get_number_list("test", "dipole");
		QI = placet_get_number_list("test", "quadrupole");
		placet_element_set_attribute("test", DI, "strength_x", 0.0);
		placet_element_set_attribute("test", DI, "strength_y", 0.0);
		placet_element_set_attribute("test", BI, "resolution", $bpmres);
		placet_element_set_attribute("test", BI, "x", randn(size(BI)) * $sigma);
		placet_element_set_attribute("test", BI, "y", randn(size(BI)) * $sigma);
		placet_element_set_attribute("test", QI, "x", randn(size(QI)) * $sigma);
		placet_element_set_attribute("test", QI, "y", randn(size(QI)) * $sigma);
		placet_element_set_attribute("test", MI, "x", randn(size(MI)) * $sigma);
		placet_element_set_attribute("test", MI, "y", randn(size(MI)) * $sigma);
	}
}

Octave {
	emittH = zeros($nm, 7);
	emittV = zeros($nm, 7);
	sizeH = zeros($nm, 7);
	sizeV = zeros($nm, 7);
	Lumi = zeros($nm, 7);
	IP = placet_get_name_number_list("test", "IP");
}

Octave {
	global Log;
	Log.Chi = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_R = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_ETA = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_CB1 = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.Chi_CB2 = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.EmittX = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.EmittY = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.SizeX = zeros(4 * ($iter1 + $iter2) + 4, 1);
	Log.SizeY = zeros(4 * ($iter1 + $iter2) + 4, 1);
}

if { $machine == "all" } {
	set machine_start 1
	set machine_end	$nm
} {
	set machine_start $machine
	set machine_end	$machine
}

puts "initial luminosity"


	Octave {
		[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
		L= get_lumi(B)
	}

Octave {
	 iteration = 1;
}

for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {
		
	my_survey

	Octave {
		[E,B] = placet_test_no_correction("test", "beam0t", "None",1,0,IP);
		L= get_lumi(B)
	iteration=iteration+1;
	}



}

set nm [expr $machine_end - $machine_start + 1]

