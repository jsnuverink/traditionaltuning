Octave {
    function Eta = MeasureDispersion(Beamline, Beam1, Beam2, delta)
    placet_test_no_correction(Beamline, Beam2, "None"); Eta  = placet_get_bpm_readings(Beamline);
    placet_test_no_correction(Beamline, Beam1, "None"); Eta -= placet_get_bpm_readings(Beamline);
    Eta /= 2 * delta;
    end
}

Octave {
    function CB = MeasureCouplingBetabeating(Beamline, Beam, Corrector, Leverage, Kick)
    K0=placet_element_get_attribute(Beamline, Corrector, Leverage);
    placet_element_set_attribute(Beamline, Corrector, Leverage, K0+Kick);
    placet_test_no_correction(Beamline, Beam, "None");
    CB=placet_get_bpm_readings(Beamline);
    placet_element_set_attribute(Beamline, Corrector, Leverage, K0-Kick);
    placet_test_no_correction(Beamline, Beam, "None");
    CB-=placet_get_bpm_readings(Beamline);
    placet_element_set_attribute(Beamline, Corrector, Leverage, K0);
    CB/=2*Kick;
    end
}
