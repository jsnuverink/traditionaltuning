Girder
Drift -name "DM1" -length 0.35 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM1" -synrad $quad_synrad -length 0.3 -strength [expr 0.05346683387*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM2" -length 15.11019834 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM2" -synrad $quad_synrad -length 0.3 -strength [expr -0.08673677641*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM3" -length 16.50485731 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM3" -synrad $quad_synrad -length 0.5300541732 -strength [expr 0.444937438*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM4" -length 1.233737 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QM4" -synrad $quad_synrad -length 1.158488747 -strength [expr -0.9724572339*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DM5" -length 1.091467 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFH" -synrad $quad_synrad -length 0.5 -strength [expr 0.02162653645*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 32.03175 -angle 0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.0008*0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.0432530729*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "HS5" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS1" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 2.215914266*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFH2" -synrad $quad_synrad -length 1 -strength [expr 0.0432530729*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "HS2" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 0.06325724469*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS5" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.0432530729*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 32.03175 -angle 0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.0008*0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFH2" -synrad $quad_synrad -length 1 -strength [expr 0.0432530729*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 32.03175 -angle 0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.0008*0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.0432530729*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "HS6" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS3" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 2.233519377*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFH2" -synrad $quad_synrad -length 1 -strength [expr 0.0432530729*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "HS4" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 0.04292297882*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRH" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "HS6" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDH2" -synrad $quad_synrad -length 1 -strength [expr -0.0432530729*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMH" -synrad $sbend_synrad -length 32.03175 -angle 0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*0.0008*0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFH" -synrad $quad_synrad -length 0.5 -strength [expr 0.02162653645*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Bpm
Quadrupole -name "QDV" -synrad $quad_synrad -length 0.5 -strength [expr -0.02162643258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 32.03175 -angle -0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0008*-0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.04325286516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS5" -synrad $mult_synrad -type 3 -length 0 -strength [expr -1.199064701*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS1" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 5.165482856*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDV2" -synrad $quad_synrad -length 1 -strength [expr -0.04325286516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS2" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 4.82680017*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS5" -synrad $mult_synrad -type 3 -length 0 -strength [expr -1.199064701*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.04325286516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 32.03175 -angle -0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0008*-0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDV2" -synrad $quad_synrad -length 1 -strength [expr -0.04325286516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 32.03175 -angle -0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0008*-0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.04325286516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS6" -synrad $mult_synrad -type 3 -length 0 -strength [expr -1.151823975*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS3" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 5.043040111*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDV2" -synrad $quad_synrad -length 1 -strength [expr -0.04325286516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Multipole -name "VS4" -synrad $mult_synrad -type 3 -length 0.5 -strength [expr 4.968579088*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DRV" -length 31.53175 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "VS6" -synrad $mult_synrad -type 3 -length 0 -strength [expr -1.151823975*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFV2" -synrad $quad_synrad -length 1 -strength [expr 0.04325286516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Sbend -name "BMV" -synrad $sbend_synrad -length 32.03175 -angle -0.0008 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0008*-0.0008/32.03175*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Bpm
Quadrupole -name "QDV" -synrad $quad_synrad -length 0.5 -strength [expr -0.02162643258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1" -length 0.85 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "Q1" -synrad $quad_synrad -length 0.3 -strength [expr 0.09063014728*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2" -length 1.338853163 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "Q2" -synrad $quad_synrad -length 0.3 -strength [expr -0.09138886841*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3" -length 85.50564653 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "Q3" -synrad $quad_synrad -length 0.8836344811 -strength [expr 0.4238510959*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D4" -length 0.35 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "Q4" -synrad $quad_synrad -length 1.126305135 -strength [expr -0.5402524188*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D5" -length 4.3 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
